
#include "zgedge.h"
#include <QPainter>
#include <QStyleOption>
#include <QDebug>
#include <QGraphicsScene>
#include "math.h"

/**
    zGEdge Constructor. \n
    Private variables are initialized here, also
    time of the animation is set by default in 0.5 seconds.

    @param edge is where all the information of the edge to be painted is saved.
    @param graphicNodeMap all nodes in scene, this way the
    edge connects the nodes by their anchor points.

*/
zGEdge::zGEdge(zEdge * edge, QMap<int,zGNode*> *graphicNodeMap)
{
    Pi = 3.14;
    qreal duration = 500;

    animationxCordNodeA = new QPropertyAnimation(this,"xCordNodeA");
    animationxCordNodeB = new QPropertyAnimation(this,"xCordNodeB");
    animationyCordNodeA = new QPropertyAnimation(this,"yCordNodeA");
    animationyCordNodeB = new QPropertyAnimation(this,"yCordNodeB");
    animationxCordNodeA->setDuration(duration);
    animationxCordNodeB->setDuration(duration);
    animationyCordNodeA->setDuration(duration);
    animationyCordNodeB->setDuration(duration);

    this->edge = edge;
    this->graphicNodeMap = graphicNodeMap;
    setFlag(ItemIsSelectable,false);
    firstTime = true;

}

zGEdge::~zGEdge()
{

}
/**
    The canvas where the edge can be painted. \n
    It is set  by default from the point [0,0] to the point [100000,100000]. \n
    Called each time paint  function is called.

*/
QRectF zGEdge::boundingRect() const
{
    return QRectF(0, 0, 100000, 100000);
}
/**
    Sets the shape of the zGEdge. \n
    By changing this function zGEdge could be selected.

*/
QPainterPath zGEdge::shape() const
{
    QPainterPath path;
    path.addRect(0,0,0,0);

    return path;
}

/**
    Recursive function which evaluates which node is being painted in the scene that can be connected to the edge.

    @param node Is the node which is being evaluated.
    @return the The function returns the node that can be connected to the zGEdge

*/
zGNode* zGEdge::nodeToDraw(zGNode *node)
{
    QList <zPrimitive*> *primitiveList = node->getNode()->getElementsList();
    for(int primitiveListIndex = 0; primitiveListIndex < primitiveList->size();primitiveListIndex++){
        if(node->getLod() >= (*primitiveList)[primitiveListIndex]->getZoomStart() +0.1  && node->getLod() <= (*primitiveList)[primitiveListIndex]->getZoomEnd()){
            return node;
        }
    }
    if(node->getFatherId() == 0){
        return node;
    }else{
        return nodeToDraw((*graphicNodeMap)[node->getFatherId()]);
    }

}
/**
    Paints the zGEdge into scene.
    @param painter The painter used to draw all the lines and text specified in the zGEdge.

*/
void zGEdge::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                   QWidget *){

    //General flags
    bool paintText = true;
    bool paintText1 = true;
    bool paintText2 = true;

    //PEN AND FONT
    QColor *arrowColor = new QColor(this->edge->getArrowColor());
    QColor *msgColor = new QColor(this->edge->getMsgColor());

    qreal newLodSize = 20;
    qreal lod = option->levelOfDetailFromTransform(painter->worldTransform());


    QFont *font = new QFont("Buxton Sketch");
    qreal valueFont = (newLodSize+lod)/(lod*1.4);
    font->setPointSizeF(valueFont);

    QPen pen(*arrowColor);
    pen.setCosmetic(true);
    pen.setWidth(1);
    QPen penMsg(*msgColor);

    //Size Messages
    QFontMetrics metrics(*font);
    qreal size1 = metrics.width(this->edge->getMessage1());
    qreal size2 = metrics.width(this->edge->getMessage2());

    arrowSize = newLodSize/lod;


    //Find nodes to connect
    zGNode * a = graphicNodeMap->value(edge->getIdFirstNode());
    zGNode * b = graphicNodeMap->value(edge->getIdSecondNode());
    if (a != nodeToDraw(a) || b != nodeToDraw(b))
    {
        paintText = false;
    }
    a = nodeToDraw(a);
    b = nodeToDraw(b);

    //Find minimun path to draw
    QPointF* anchorPointsA = a->getAnchorPointArray();
    QPointF* anchorPointsB = b->getAnchorPointArray();
    qreal minDistance = sqrt(pow(anchorPointsA[0].x()-anchorPointsB[0].x(),2)+pow(anchorPointsA[0].y()-anchorPointsB[0].y(),2));
    int indexA = 0;
    int indexB = 0;
    for(int i = 0; i < 4 ;i++){
        for(int j = 0; j < 4 ;j++){
            qreal distanceVar = sqrt(pow(anchorPointsA[i].x()-anchorPointsB[j].x(),2)+pow(anchorPointsA[i].y()-anchorPointsB[j].y(),2));
            if(minDistance > distanceVar ){
                minDistance = distanceVar;
                indexA = i;
                indexB = j;
            }
        }
    }

    //Lines to create edges and arrows
    QLineF lineToPaint (anchorPointsA[indexA],anchorPointsB[indexB]);
    QLineF lineToPaint2 (anchorPointsB[indexB],anchorPointsA[indexA]);

    //Animation
    if(firstTime){
        firstTime = false;
        oldLine = lineToPaint;
    }else
    {

        if(oldLine != lineToPaint){
            if(lineToPaint.p1() == lineToPaint.p2()){
                animationxCordNodeA->setStartValue(oldLine.p1().x());
                animationxCordNodeB->setStartValue(oldLine.p2().x());
                animationyCordNodeA->setStartValue(oldLine.p1().y());
                animationyCordNodeB->setStartValue(oldLine.p2().y());

                animationxCordNodeA->setEndValue((oldLine.p1().x()+ oldLine.p2().x())/2);
                animationxCordNodeB->setEndValue((oldLine.p1().x()+ oldLine.p2().x())/2);
                animationyCordNodeA->setEndValue((oldLine.p1().y()+ oldLine.p2().y())/2);
                animationyCordNodeB->setEndValue((oldLine.p1().y()+ oldLine.p2().y())/2);

            }else if(oldLine.p1() == oldLine.p2()){
                animationxCordNodeA->setStartValue((lineToPaint.p1().x()+ lineToPaint.p2().x())/2);
                animationxCordNodeB->setStartValue((lineToPaint.p1().x()+ lineToPaint.p2().x())/2);
                animationyCordNodeA->setStartValue((lineToPaint.p1().y()+ lineToPaint.p2().y())/2);
                animationyCordNodeB->setStartValue((lineToPaint.p1().y()+ lineToPaint.p2().y())/2);

                animationxCordNodeA->setEndValue(lineToPaint.p1().x());
                animationxCordNodeB->setEndValue(lineToPaint.p2().x());
                animationyCordNodeA->setEndValue(lineToPaint.p1().y());
                animationyCordNodeB->setEndValue(lineToPaint.p2().y());

            }else{
                animationxCordNodeA->setStartValue(oldLine.p1().x());
                animationxCordNodeB->setStartValue(oldLine.p2().x());
                animationyCordNodeA->setStartValue(oldLine.p1().y());
                animationyCordNodeB->setStartValue(oldLine.p2().y());

                animationxCordNodeA->setEndValue(lineToPaint.p1().x());
                animationxCordNodeB->setEndValue(lineToPaint.p2().x());
                animationyCordNodeA->setEndValue(lineToPaint.p1().y());
                animationyCordNodeB->setEndValue(lineToPaint.p2().y());
            }
            animationxCordNodeA->start();
            animationxCordNodeB->start();
            animationyCordNodeA->start();
            animationyCordNodeB->start();
            oldLine =lineToPaint;
        }

    }

    //Arrow creation

    double angleLine1 = ::acos(lineToPaint.dx() / minDistance);
    double angleLine2 = ::acos(lineToPaint2.dx() / minDistance);
    lines.clear();
    QPointF arrowP1;
    QPointF arrowP2;
    QPointF arrowP3;
    QPointF arrowP4;

    if (lineToPaint.dy() >= 0)
        angleLine1 = ( Pi * 2) - angleLine1;
    if (lineToPaint2.dy() >= 0)
        angleLine2 = ( Pi * 2) - angleLine2;

    //0 no arrow
    //1 from 1 to 2
    //2 from 2 to 1
    //3 arrow in both directions
    //4 no arrow & dashLine
    //5 from 1 to 2 & dashLine
    //6 from 2 to 1 & dashLine
    //7 arrow in both directions & dashLine

    if(this->edge->getDirection() == 2
            || this->edge->getDirection() == 3
                || this->edge->getDirection() == 6
                    || this->edge->getDirection() == 7){


        arrowP1 = lineToPaint.p1() + QPointF(sin(angleLine1 + Pi / 3) * arrowSize,
                                             cos(angleLine1 + Pi / 3) * arrowSize);
        arrowP2 = lineToPaint.p1() + QPointF(sin(angleLine1 + Pi - Pi / 3) * arrowSize,
                                             cos(angleLine1 + Pi - Pi / 3) * arrowSize);
        lines.push_back(QLineF(lineToPaint.p1(),arrowP1));
        lines.push_back(QLineF(lineToPaint.p1(),arrowP2));

    }

    if(this->edge->getDirection() == 1
            || this->edge->getDirection() == 3
                || this->edge->getDirection() == 5
                    || this->edge->getDirection() == 7){


        arrowP3 = lineToPaint2.p1() + QPointF(sin(angleLine2 + Pi / 3) * arrowSize,
                                              cos(angleLine2 + Pi / 3) * arrowSize);
        arrowP4 = lineToPaint2.p1() + QPointF(sin(angleLine2 + Pi - Pi / 3) * arrowSize,
                                              cos(angleLine2 + Pi - Pi / 3) * arrowSize);
        lines.push_back(QLineF(lineToPaint2.p1(),arrowP3));
        lines.push_back(QLineF(lineToPaint2.p1(),arrowP4));
    }

    //Message location verification (max multiplier)

    qreal m1 = (anchorPointsB[indexB].y() - anchorPointsA[indexA].y())/
            (anchorPointsB[indexB].x() - anchorPointsA[indexA].x());
    qreal angleRotation = ::atan(m1);
    qreal  posmsg1 = this->edge->getPosmsg1();
    qreal  posmsg2 = this->edge->getPosmsg2();
    QPointF transalatePoint;
    QPointF transalatePoint2;
    qreal distance = minDistance - size1 - 3;
    qreal distance2 = minDistance - size2 - 3;
    qreal posmax = distance/minDistance;
    qreal posmax2 = distance2/minDistance;
    qreal posmin = 1 - posmax;
    qreal posmin2 = 1 - posmax2;

    if(anchorPointsA[indexA].x() <= anchorPointsB[indexB].x()){

        if(posmsg1 > posmax){
            posmsg1 = posmax;
        }

        if(posmsg2 < posmin2){
            posmsg2 = posmin2;
        }

        if(posmsg1 == 0)
            posmsg1 = 0.05;

        if(posmsg2 == 1)
            posmsg2 = 0.95;
    }
    else{

        if(posmsg2 > posmax2){
            posmsg2 = posmax2;
        }

        if(posmsg1 < posmin){
            posmsg1 = posmin;
        }

        if(posmsg2 == 0)
            posmsg2 = 0.05;

        if(posmsg1 == 1)
            posmsg1 = 0.95;

    }

    //Message location
    if(animationxCordNodeA->state() == QAbstractAnimation::Running){
        anchorPointsB[indexB] = QPointF(xCordNodeB,yCordNodeB);
        anchorPointsA[indexA] = QPointF(xCordNodeA,yCordNodeA);
    }

        if(anchorPointsA[indexA].x() <  anchorPointsB[indexB].x()){
            transalatePoint.setX(anchorPointsA[indexA].x() + ((qMax(anchorPointsA[indexA].x(),anchorPointsB[indexB].x()) - qMin(anchorPointsA[indexA].x(),anchorPointsB[indexB].x())) *posmsg1) );
            transalatePoint2.setX(anchorPointsB[indexB].x() - ((qMax(anchorPointsA[indexA].x(),anchorPointsB[indexB].x()) - qMin(anchorPointsA[indexA].x(),anchorPointsB[indexB].x())) *posmsg2) );
        }
        else{
            transalatePoint.setX(anchorPointsA[indexA].x() - ((qMax(anchorPointsA[indexA].x(),anchorPointsB[indexB].x()) - qMin(anchorPointsA[indexA].x(),anchorPointsB[indexB].x())) *posmsg1) );
            transalatePoint2.setX(anchorPointsB[indexB].x() + ((qMax(anchorPointsA[indexA].x(),anchorPointsB[indexB].x()) - qMin(anchorPointsA[indexA].x(),anchorPointsB[indexB].x())) *posmsg2) );
        }

        if(anchorPointsA[indexA].x() ==  anchorPointsB[indexB].x()){
            if(anchorPointsA[indexA].y() <  anchorPointsB[indexB].y()){
                transalatePoint.setY( anchorPointsA[indexA].y() + ((qMax(anchorPointsA[indexA].y(),anchorPointsB[indexB].y()) - qMin(anchorPointsA[indexA].y(),anchorPointsB[indexB].y())) *posmsg1));
                transalatePoint2.setY(anchorPointsB[indexB].y() - ((qMax(anchorPointsA[indexA].y(),anchorPointsB[indexB].y()) - qMin(anchorPointsA[indexA].y(),anchorPointsB[indexB].y())) *posmsg2));
            }else{
                transalatePoint.setY( anchorPointsA[indexA].y() - ((qMax(anchorPointsA[indexA].y(),anchorPointsB[indexB].y()) - qMin(anchorPointsA[indexA].y(),anchorPointsB[indexB].y())) *posmsg1));
                transalatePoint2.setY(anchorPointsB[indexB].y() + ((qMax(anchorPointsA[indexA].y(),anchorPointsB[indexB].y()) - qMin(anchorPointsA[indexA].y(),anchorPointsB[indexB].y())) *posmsg2));
            }
        }else{
            transalatePoint.setY(m1 * (transalatePoint.x() - anchorPointsA[indexA].x()) + anchorPointsA[indexA].y());
            transalatePoint2.setY(m1 * (transalatePoint2.x() - anchorPointsB[indexB].x()) + anchorPointsB[indexB].y());

        }

    if( this->edge->getDirection() >= 4 && this->edge->getDirection() <= 7)
        pen.setStyle(Qt::DashLine);

    //Paint into scene

    painter->setPen(pen);
    painter->setFont(*font);

    if(animationxCordNodeA->state() == QAbstractAnimation::Running){
         painter->drawLine(QPointF(xCordNodeA,yCordNodeA),QPointF(xCordNodeB,yCordNodeB));
    }else{
        painter->drawLine(oldLine);
        if(lod < 0.5){

        }else{
             painter->drawLines(lines);
        }

    }

    painter->setPen(penMsg);

    if(size1 - 3 > minDistance){
        paintText1 = false;
    }
    if(size2 - 3 > minDistance){
        paintText2 = false;
    }
    qreal angleRotationCopy = angleRotation;
    if(paintText){
        if(angleRotation > 0)
            angleRotation = angleRotation + 2* Pi;

        angleRotation = angleRotation * 180/Pi;

        if(paintText1){
            QRectF *spaceText = new QRectF(0,0,size1+1,20);
            painter->translate(QPointF(transalatePoint.x() + 20*sin(angleRotationCopy),
                                       transalatePoint.y() - 20*cos(angleRotationCopy)));
            painter->rotate(angleRotation);

            painter->drawText(*spaceText,Qt::AlignCenter|Qt::AlignBottom,this->edge->getMessage1());

            painter->rotate(-angleRotation);
            painter->translate(QPointF( -(transalatePoint.x() + 20*sin(angleRotationCopy)),
                                                   -(transalatePoint.y() - 20*cos(angleRotationCopy))));

            delete spaceText;
        }
        if(paintText2){
            QRectF *spaceText2 = new QRectF(0,0,size2+1,20);
            painter->translate(QPointF(transalatePoint2.x() + 20*sin(angleRotationCopy),
                                       transalatePoint2.y() - 20*cos(angleRotationCopy)));

            painter->rotate(angleRotation);
            painter->drawText(*spaceText2,Qt::AlignCenter|Qt::AlignBottom,this->edge->getMessage2());

            delete spaceText2;
        }
    }

    delete arrowColor ;
    delete msgColor;
    delete font ;

    return;
}
void zGEdge::setXCordNodeA(qreal xCordNodeA){
    this->xCordNodeA = xCordNodeA;
    update();
}
void zGEdge::setYCordNodeA(qreal yCordNodeA){
    this->yCordNodeA = yCordNodeA;
    update();
}
void zGEdge::setXCordNodeB(qreal xCordNodeB){
    this->xCordNodeB = xCordNodeB;
    update();
}
void zGEdge::setYCordNodeB(qreal yCordNodeB){
    this->yCordNodeB = yCordNodeB;
    update();
}
qreal zGEdge::getXCordNodeA(){
    return this->xCordNodeA;
}
qreal zGEdge::getYCordNodeA(){
    return this->yCordNodeA;
}
qreal zGEdge::getXCordNodeB(){
    return this->xCordNodeB;
}
qreal zGEdge::getYCordNodeB(){
    return this->yCordNodeB;
}
