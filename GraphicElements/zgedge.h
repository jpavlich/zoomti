#ifndef ZGEDGE_H
#define ZGEDGE_H

#include <QGraphicsObject>
#include "../zedge.h"
#include <QtGlobal>

class zGEdge :  public QGraphicsObject
{
    Q_OBJECT
    Q_PROPERTY(qreal xCordNodeA READ getXCordNodeA WRITE setXCordNodeA )
    Q_PROPERTY(qreal xCordNodeB READ getXCordNodeB WRITE setXCordNodeB )

    Q_PROPERTY(qreal yCordNodeA READ getYCordNodeA WRITE setYCordNodeA )
    Q_PROPERTY(qreal yCordNodeB READ getYCordNodeB WRITE setYCordNodeB )


public:

    zGEdge(zEdge * edge, QMap<int,zGNode*> *graphicNodeMap);
    ~zGEdge();
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                   QWidget *widget) Q_DECL_OVERRIDE;
    zGNode* nodeToDraw(zGNode *node);
    qreal getXCordNodeA();
    qreal getYCordNodeA();
    qreal getXCordNodeB();
    qreal getYCordNodeB();
    void setXCordNodeA(qreal xCordNodeA);
    void setYCordNodeA(qreal yCordNodeA);
    void setXCordNodeB(qreal xCordNodeB);
    void setYCordNodeB(qreal yCordNodeB);

private:
    bool firstTime;
    zEdge * edge;
    QMap<int,zGNode*> *graphicNodeMap;
    QPropertyAnimation *animationxCordNodeA;
    QPropertyAnimation *animationxCordNodeB;
    QPropertyAnimation *animationyCordNodeA;
    QPropertyAnimation *animationyCordNodeB;
    qreal Pi;
    qreal arrowSize;
    qreal xCordNodeA;
    qreal yCordNodeA;
    qreal xCordNodeB;
    qreal yCordNodeB;
    QVector<QLineF> lines;
    QLineF oldLine;


};

#endif // ZGEDGE_H
