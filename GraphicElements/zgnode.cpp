#include "zgnode.h"
#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>
#include <QDebug>
#include <math.h>
#include <qfontdatabase.h>

/**
    zGNode Constructor. \n
    Private variables and flags of the ZGNode are initialized here.


    @param node is where all the information of the node to be painted is saved.
    @param width Width of the node.
    @param height Height of the node
    @param fatherScale The scale of  the zGNode container.
    @param fatherXPos The x coordinate of the zGNode container.
    @param fatherYPos The y coordinate of the zGNode container.

*/
zGNode::zGNode(zNode* node, int width, int height, qreal fatherScale, qreal fatherXPos, qreal fatherYPos)
{
    drawPainter = false;
    fatherScaleVal = fatherScale;
    maxXEllipse = false;
    maxYEllipse = false;
    this->setOpacity(0.001);
    firstPaint = false;

    QList < zPrimitive*> *primitives = node->getElementsList();
    minLod = (*primitives)[0]->getZoomStart();
    maxLod = (*primitives)[0]->getZoomEnd();
    for(int primitiveIndex = 0; primitiveIndex < primitives->size();primitiveIndex++){
        if((*primitives)[primitiveIndex]->getZoomStart() < minLod){
            minLod = (*primitives)[primitiveIndex]->getZoomStart();
        }
        if((*primitives)[primitiveIndex]->getZoomEnd() > maxLod){
            maxLod = (*primitives)[primitiveIndex]->getZoomEnd();
        }
    }

    //ANIMATION
    animation = new QPropertyAnimation(this,"opacity");
    animation->setStartValue(0);
    animation->setEndValue(1);

    animation->setDuration(500);
    animation->setEasingCurve(QEasingCurve::InBack);

    animationFinalX = new QPropertyAnimation(this,"pos");
    animationFinalX->setDuration(500);

    //ANCHOR POINTS
    QPointF anchorPoint(-1,-1);
    for(int arrayIndex = 0; arrayIndex < 4; arrayIndex++ ){
        anchorPointArray[arrayIndex] = anchorPoint;
    }

    this->maxX =0;
    this->maxY =0;
    this->centerX = 0;
    this->centerY = 0;


    this->node = node;
    setFlags(ItemIsSelectable);// | ItemIsMovable);

    QList<zPrimitive*> *nodePrimitives = this->node->getElementsList();
    nodeXPos = this->getNode()->getXCord();
    nodeYPos = this->getNode()->getYCord();


    fatherId = this->node->getFatherId();

    for(int nodeListIndex = 0; nodeListIndex < nodePrimitives->size(); nodeListIndex++){
        int localType = (*nodePrimitives)[nodeListIndex]->getType();
        //TODO Others
        if(localType == rectangle){

            zRectangle * tempRect =(zRectangle*)(*nodePrimitives)[nodeListIndex];


            if((tempRect->getWidth()+tempRect->getX()) * scale() > maxX){

                maxX = (tempRect->getWidth()+tempRect->getX()) * scale();
                maxXEllipse = false;
            }
            if((tempRect->getHeight()+tempRect->getY()) *scale() > maxY){

                maxY = (tempRect->getHeight()+tempRect->getY()) *scale();
                maxYEllipse = false;

            }
        }else if(localType == ellipse){

            zEllipse * tempEllipse =(zEllipse*)(*nodePrimitives)[nodeListIndex];

            if((tempEllipse->getWidth()+tempEllipse->getX()) * scale() > maxX){
                maxXEllipse = true;
                maxX = ((tempEllipse->getWidth())+tempEllipse->getX()) * scale();
                centerX = tempEllipse->getX();
            }
            if((tempEllipse->getHeight()+tempEllipse->getY()) *scale() > maxY){
                maxYEllipse = true;
                maxY = ((tempEllipse->getHeight())+tempEllipse->getY()) *scale();
                centerY = tempEllipse->getY();
            }
        }
        else if(localType == twinLine){

            ztwinline * tempTwinLine =(ztwinline*)(*nodePrimitives)[nodeListIndex];

            qreal lWidth =0 , lHeight =0;
            lWidth =  qMax(tempTwinLine->getXe(),tempTwinLine->getX2e()) - qMin(tempTwinLine->getX(),tempTwinLine->getX2());
            lHeight = qMax(tempTwinLine->getYe(),tempTwinLine->getY2e()) - qMin(tempTwinLine->getY(),tempTwinLine->getY2());

            if(lWidth * scale() > maxX){
                maxX = lWidth * scale();
                maxXEllipse = false;
            }
            if(lHeight *scale() > maxY){
                maxY = lHeight *scale();
                maxYEllipse = false;
            }
        }
        else if(localType == image){

            zImage * tempImage =(zImage*)(*nodePrimitives)[nodeListIndex];
            if((tempImage->getWidth()+tempImage->getX()) * scale() > maxX){

                maxX = (tempImage->getWidth()+tempImage->getX()) * scale();

            }
            if((tempImage->getHeight()+tempImage->getY()) *scale() > maxY){

                maxY = (tempImage->getHeight()+tempImage->getY()) *scale();

            }
        }
    }

    //If getWContainer() and getHContainer are different from -1 the node should be scaled
    if(node->getWContainer() != -1 && node->getHContainer() != -1){

        nodeXPos = fatherXPos + (this->getNode()->getXCord()  - this->getNode()->getFatherAbsXCord())*fatherScale;
        nodeYPos = fatherYPos + (this->getNode()->getYCord()  - this->getNode()->getFatherAbsYCord())*fatherScale;

        qreal wDiv = maxX / (qreal)width;
        qreal hDiv = maxY / (qreal)height;

        qreal maxDiv = qMax(wDiv,hDiv);
        qreal newScale = 1;

        if(maxDiv == wDiv){
            newScale = (maxDiv * (qreal)node->getWContainer()) / (1.5* (maxX) );
        }else{
            newScale = (maxDiv * (qreal)node->getHContainer()) / (1.5* (maxY) );
        }

        setScale(fatherScale*newScale);

    }else{
        nodeXPos = this->getNode()->getXCord();
        nodeYPos = this->getNode()->getYCord();
    }

    qreal difX = width / maxX*scale();
    qreal difY = height/maxY*scale();
    qreal dif = qMin(difX,difY);
    nodeScaleFactor = 0;

    int elipseXValue = 1;
    int elipseYValue = 1;

    if(difX == dif){
        nodeScaleFactor = (qreal)(width*elipseXValue - 10)/(qreal)(maxX*scale());
    }else{
        nodeScaleFactor = (qreal)(height*elipseYValue - 10)/(qreal)(maxY*scale());
    }

    setData(1,nodeScaleFactor);
    setData(2,node->getNId());
    setData(3,1);

    if( (maxXEllipse && maxYEllipse)
            || (maxXEllipse && maxX > maxY)
            || (maxYEllipse && maxY > maxX) ){
        setData(4,nodeXPos  + ((centerX*scale())));
        setData(5,nodeYPos  + ((centerY*scale())));
        setData(6,nodeXPos  + ((centerX*scale())));
        setData(7,nodeYPos  + ((centerY*scale())));
    }else{
        setData(4,nodeXPos  + ((maxX*scale()/2.0f)));
        setData(5,nodeYPos  + ((maxY*scale()/2.0f)));
        setData(6,nodeXPos  + ((maxX*scale()/2.0f)));
        setData(7,nodeYPos  + ((maxY*scale()/2.0f)));
    }

    setData(4,nodeXPos  + ((maxX*scale()/2.0f)));
    setData(5,nodeYPos  + ((maxY*scale()/2.0f)));
    setData(6,nodeXPos  + ((maxX*scale()/2.0f)));
    setData(7,nodeYPos  + ((maxY*scale()/2.0f)));

    //ANCHOR
    lastLod = -1 ;
    this->trigger = "null";

}
zGNode::zGNode()
{


}
/**
    Destructor of zGNode. \n
*/
zGNode::~zGNode()
{
    delete node;
}
/**
    The canvas where the edge can be painted. \n
    It is set  by default from the point [0,0] to the point [1000,1000]. \n
    Called each time paint  function is called.

*/
QRectF zGNode::boundingRect() const
{
    return QRectF(0, 0, 1000, 1000);
}
/**
    Sets the path made by the rectangles who are being showed in the scene of the ZGNode.\n
    @param path The path of the zGNode.
    @param rectangle The rectangle to be added to the path

*/
void zGNode::pathRectangle(QPainterPath* path, zRectangle* rectangle) const
{
    if(lod >= rectangle->getZoomStart() && lod < rectangle->getZoomEnd()){

        if(rectangle->getRadius() == 0){
            path->addRect(rectangle->getX(),
                          rectangle->getY(),
                          rectangle->getWidth(),
                          rectangle->getHeight());
        }
        else {
            path->addRoundedRect(rectangle->getX(),
                                rectangle->getY(),
                                rectangle->getWidth(),
                                rectangle->getHeight(),
                                rectangle->getRadius(),
                                rectangle->getRadius());

            if(!rectangle->getCurveTopLeft()){
                path->addRect( QRect( rectangle->getX(),rectangle->getY(),rectangle->getRadius() + 10,rectangle->getRadius() + 10) );
            }

            if(!rectangle->getCurveTopRight()){
                path->addRect( QRect( rectangle->getX() + rectangle->getWidth() - rectangle->getRadius() - 10,rectangle->getY(),rectangle->getRadius() + 10,rectangle->getRadius() + 10) );
            }

            if(!rectangle->getCurveBottomLeft()){
                path->addRect( QRect( rectangle->getX(),rectangle->getY() + rectangle->getHeight() - rectangle->getRadius() - 10,rectangle->getRadius() + 10,rectangle->getRadius() + 10) );
            }

            if(!rectangle->getCurveBottomRight()){
                path->addRect( QRect( rectangle->getX() + rectangle->getWidth() - rectangle->getRadius() - 10,rectangle->getY() + rectangle->getHeight() - rectangle->getRadius() - 10,rectangle->getRadius() + 10,rectangle->getRadius() + 10) );
            }

        }
    }

}
/**
    Sets the path made by the ellipses who are being showed in the scene of the ZGNode.\n
    @param path The path of the zGNode.
    @param ellipse The ellipse to be added to the path

*/
void zGNode::pathEllipse(QPainterPath* path, zEllipse* ellipse) const
{
    if(lod >= ellipse->getZoomStart() && lod < ellipse->getZoomEnd()){
        path->addEllipse(ellipse->getX(),ellipse->getY(),
                      ellipse->getWidth(),
                      ellipse->getHeight());
    }

}
/**
    Sets the path made by the Images who are being showed in the scene of the ZGNode.\n
    @param path The path of the zGNode.
    @param image The image to be added to the path

*/
void zGNode::pathImage(QPainterPath* path, zImage* image) const
{
    if(lod >= image->getZoomStart() && lod < image->getZoomEnd()){
        path->addRect(image->getX(),image->getY(),
                      image->getWidth(),
                      image->getHeight());
    }

}
/**
    Sets the path made by the TwinLines who are being showed in the scene of the ZGNode.\n
    @param path The path of the zGNode.
    @param twinline The twinline to be added to the path

*/
void zGNode::pathTwinLine(QPainterPath* path, ztwinline* twinline) const
{
    if(lod >= twinline->getZoomStart() && lod < twinline->getZoomEnd()){

        qreal width =0 , height =0;
        width =  qMax(twinline->getXe(),twinline->getX2e()) - qMin(twinline->getX(),twinline->getX2());
        height = qMax(twinline->getYe(),twinline->getY2e()) - qMin(twinline->getY(),twinline->getY2());

        int xM = qMin(twinline->getX(),twinline->getX2());
        int yM = qMin(twinline->getY(),twinline->getY2());


        path->addRect(xM,
                      yM,
                      width,
                      height);
    }

}
/**
    Sets the path of the ZGNode.\n
    This funcion calls the path of the different primivites.

    @return QPainterPath The complete path of the zGNode.

*/
QPainterPath zGNode::shape() const
{

    QPainterPath path;
    path.setFillRule(Qt::WindingFill);

    QList<zPrimitive*> *nodePrimitiveList = this->node->getElementsList();

    for(int nodeListIndex = 0; nodeListIndex < nodePrimitiveList->size(); nodeListIndex++){
        int localType = (*nodePrimitiveList)[nodeListIndex]->getType();
        //TODO Others
        if(localType == rectangle){
            pathRectangle(&path,(zRectangle*)(*nodePrimitiveList)[nodeListIndex]);
        }
        else if(localType == ellipse){
            pathEllipse(&path,(zEllipse*)(*nodePrimitiveList)[nodeListIndex]);
        }
        else if(localType == twinLine){
            pathTwinLine(&path,(ztwinline*)(*nodePrimitiveList)[nodeListIndex]);
        }
        else if(localType == image){
            pathImage(&path,(zImage*)(*nodePrimitiveList)[nodeListIndex]);
        }
    }

    return path;
}

/**
    Paints the texts of the ZGNode.\n
    @param painter The painter of the zGNode which paints the text.
    @param ztextVector The text to be painted.
    @param width The width of the container.
    @param height The height of the container.
    @param type The type of the container.

*/
void zGNode::paintText(QPainter *painter, QVector<ztext*> *ztextVector, qreal width, qreal height, int type = 0  ){


    QPen oldPen(painter->pen());

    QColor *color = new QColor(Qt::black);

    QPen pen(*color);
    pen.setCosmetic(true);

    painter->setPen(pen);

    QFont *font = new QFont("Buxton Sketch");
    painter->setFont(*font);

    float absMin = 99999;

    for(int textIndex = 0; textIndex < ztextVector->size(); textIndex++){

        if(lod >= (*ztextVector)[textIndex]->getZoomStart() && lod <  (*ztextVector)[textIndex]->getZoomEnd()){

            qreal maxWidth = 0, maxHeight = 0;
            if(this->node->getTwoLinesText()){
                QString localWord = (*ztextVector)[textIndex]->getText();

                int maxSIndex = -1;
                int maxEIndex = -1;

                int startIndex = 0;
                int endIndex = 0;
                for(int wordIndex = 0; wordIndex < localWord.size(); wordIndex++){
                    if(localWord[wordIndex] != ' '){
                        endIndex=wordIndex;
                    }
                    else
                    {
                        if(maxEIndex - maxSIndex < endIndex - startIndex){
                            maxEIndex = endIndex;
                            maxSIndex = startIndex;
                        }
                        startIndex = wordIndex+1;
                        endIndex = startIndex;
                    }
                }

                if(endIndex == localWord.size()-1){
                    if(maxEIndex - maxSIndex < endIndex - startIndex){
                        maxEIndex = endIndex;
                        maxSIndex = startIndex;
                    }
                }

                QRect textRect = painter->boundingRect( (*ztextVector)[textIndex]->getX(), (*ztextVector)[textIndex]->getY(),width,height,Qt::AlignCenter|Qt::TextDontClip,  localWord.mid(maxSIndex, maxEIndex-maxSIndex));
                maxWidth = textRect.width();
                maxHeight = textRect.height();

            }else{
                QRect textRect = painter->boundingRect( (*ztextVector)[textIndex]->getX(), (*ztextVector)[textIndex]->getY(),width,height,Qt::AlignCenter|Qt::TextDontClip,  (*ztextVector)[textIndex]->getText());
                maxWidth = textRect.width();
                maxHeight = textRect.height();
            }

            float xScale = width / maxWidth;
            float yScale = height / maxHeight;
            float scale = qMin(xScale,yScale);

            absMin = qMin(absMin,scale);
        }
    }

    QFont f = painter->font();
    if(absMin < 1)
        f.setPointSizeF(f.pointSizeF()*absMin);
    else{
               f.setPointSizeF(f.pointSizeF()*absMin / 2 );
               if(f.pointSize() > 30)
                   f.setPointSizeF(30);
        }

    painter->setFont(f);


    for(int textIndex = 0; textIndex < ztextVector->size(); textIndex++){

        if(lod >= (*ztextVector)[textIndex]->getZoomStart() && lod <  (*ztextVector)[textIndex]->getZoomEnd()){

            QRect textRect = painter->boundingRect( (*ztextVector)[textIndex]->getX(), (*ztextVector)[textIndex]->getY(),width,height,Qt::AlignCenter|Qt::TextDontClip,  (*ztextVector)[textIndex]->getText());

            painter->setFont(f);

            int flags = Qt::AlignCenter|Qt::TextWordWrap;

            if( (*ztextVector)[textIndex]->getAlign() == "left"){
                flags = Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap;
            }
            else if((*ztextVector)[textIndex]->getAlign() == "center"){
                flags = Qt::AlignHCenter|Qt::AlignTop|Qt::TextWordWrap;
            }
            else if((*ztextVector)[textIndex]->getAlign() == "right"){
                flags = Qt::AlignRight|Qt::AlignTop|Qt::TextWordWrap;
            }
            else if((*ztextVector)[textIndex]->getAlign() == "bottom"){
                flags = Qt::AlignHCenter|Qt::AlignBottom|Qt::TextWordWrap;
            }

            if (type == ellipse){
                  flags = Qt::AlignCenter|Qt::TextWordWrap;
            }

            if(width != -1 && height != -1){
                painter->drawText( (*ztextVector)[textIndex]->getX(),
                                   (*ztextVector)[textIndex]->getY() + textRect.height()*textIndex,
                                  width,
                                  height,
                                  flags,
                                   (*ztextVector)[textIndex]->getText());

            }else{
                painter->drawText( (*ztextVector)[textIndex]->getX(),
                                   (*ztextVector)[textIndex]->getY(),
                                   (*ztextVector)[textIndex]->getText());
            }

        }

    }


    painter->setPen(oldPen);
    delete color;
    delete font;

}
/**
    Paints the rectangles of the ZGNode.\n
    @param painter The painter of the zGNode which paints the rectangle.
    @param zrectangle The rectangle to be painted.

*/
void zGNode::paintRectangle(QPainter *painter, zRectangle* zrectangle)
{

    if(lod >= zrectangle->getZoomStart() && lod < zrectangle->getZoomEnd() ){

        if(zrectangle->getRadius() == 0){
            painter->drawRect(zrectangle->getX(),
                              zrectangle->getY(),
                              zrectangle->getWidth(),
                              zrectangle->getHeight());
        }
        else
        {
            if(!drawPainter){
                painter->drawPath( shape().simplified() );
                drawPainter = true;

                if(zrectangle->getHeight() == 60){
                    painter->drawLine(zrectangle->getX() ,
                                      zrectangle->getY() + zrectangle->getHeight(),
                                      zrectangle->getX() + zrectangle->getWidth() ,
                                      zrectangle->getY() + zrectangle->getHeight());
                }
            }
        }

        maxX = qMax(maxX,zrectangle->getWidth());
        maxY = qMax(maxY,zrectangle->getHeight());

        minX = qMin(minX,(qreal)zrectangle->getX());
        minY = qMin(minY,(qreal)zrectangle->getY());
    }

    if(!zrectangle->getContainsList()->empty()){

        QList<zPrimitive*> *containListTmp = zrectangle->getContainsList();
        QVector<ztext*> textToDraw;

        for(int containsListTmpIndex = 0; containsListTmpIndex < containListTmp->size(); containsListTmpIndex++){
            int localType = (*containListTmp)[containsListTmpIndex]->getType();
            //TODO OTHERS
            if(localType == rectangle){
                paintRectangle(painter, (zRectangle*)(*containListTmp)[containsListTmpIndex]);
            }
            else if(localType == text){
                textToDraw.push_back((ztext*)(*containListTmp)[containsListTmpIndex]);
            }
            else if(localType == ellipse){
               paintEllipse(painter, (zEllipse*)(*containListTmp)[containsListTmpIndex]);
            }
            else if(localType == twinLine){
                paintTwinLines(painter, (ztwinline*)(*containListTmp)[containsListTmpIndex]);
            }else if(localType == image){
               paintImage(painter, (zImage*)(*containListTmp)[containsListTmpIndex]);
            }
        }
        paintText(painter, &textToDraw,zrectangle->getWidth(),zrectangle->getHeight());
    }



}

/**
    Paints the images of the ZGNode.\n
    @param painter The painter of the zGNode which paints the image.
    @param zimage The image to be painted.

*/
void zGNode::paintImage(QPainter *painter, zImage* zimage)
{

    if(lod >= zimage->getZoomStart() && lod < zimage->getZoomEnd() ){

        painter->setRenderHint(QPainter::Antialiasing,true);

        painter->drawPixmap(zimage->getX(),
                            zimage->getY(),
                            zimage->getWidth(),
                            zimage->getHeight(),
                            zimage->getImg());

        painter->setRenderHint(QPainter::Antialiasing,false);


        maxX = qMax(maxX,zimage->getWidth());
        maxY = qMax(maxY,zimage->getHeight());

        minX = qMin(minX,(qreal)zimage->getX());
        minY = qMin(minY,(qreal)zimage->getY());
    }

    if(!zimage->getContainsList()->empty()){

        QList<zPrimitive*> *containListTmp = zimage->getContainsList();
        QVector<ztext*> textToDraw;

        for(int containsListTmpIndex = 0; containsListTmpIndex < containListTmp->size(); containsListTmpIndex++){
            int localType = (*containListTmp)[containsListTmpIndex]->getType();
            //TODO OTHERS
            if(localType == rectangle){
                paintRectangle(painter, (zRectangle*)(*containListTmp)[containsListTmpIndex]);
            }
            else if(localType == text){
                textToDraw.push_back((ztext*)(*containListTmp)[containsListTmpIndex]);
            }
            else if(localType == ellipse){
               paintEllipse(painter, (zEllipse*)(*containListTmp)[containsListTmpIndex]);
            }
            else if(localType == twinLine){
                paintTwinLines(painter, (ztwinline*)(*containListTmp)[containsListTmpIndex]);
            }else if(localType == image){
               paintImage(painter, (zImage*)(*containListTmp)[containsListTmpIndex]);
            }
        }
        paintText(painter, &textToDraw,zimage->getWidth(),zimage->getHeight());
    }



}
/**
    Paints the ellipses of the ZGNode.\n
    @param painter The painter of the zGNode which paints the ellipse.
    @param zellipse The ellipse to be painted.

*/
void zGNode::paintEllipse(QPainter *painter, zEllipse* zellipse)
{
    QBrush oldBrush = painter->brush();
    QPen oldPen = painter->pen();

    if(zellipse->getColor().length() > 0){
        QColor *color = new QColor(QString(zellipse->getColor()));
        QBrush brush(*color);
        painter->setBrush(brush);

        delete color;
    }


    if(lod >= zellipse->getZoomStart() && lod < zellipse->getZoomEnd()){

        painter->drawEllipse(zellipse->getX(),
                          zellipse->getY(),
                          zellipse->getWidth(),
                          zellipse->getHeight());


        maxX = qMax(maxX, zellipse->getWidth());
        maxY = qMax(maxY, zellipse->getHeight());

        minX = qMin(minX,(qreal)zellipse->getX());
        minY = qMin(minY,(qreal)zellipse->getY());
    }

    painter->setBrush(oldBrush);
    painter->setPen(oldPen);

    if(!zellipse->getContainsList()->empty()){

        QList<zPrimitive*> *containListTmp = zellipse->getContainsList();
        QVector<ztext*> textToDraw;

        for(int containsListTmpIndex = 0; containsListTmpIndex < containListTmp->size(); containsListTmpIndex++){
            int localType = (*containListTmp)[containsListTmpIndex]->getType();
            //TODO OTHERS
            if(localType == rectangle){
                paintRectangle(painter, (zRectangle*)(*containListTmp)[containsListTmpIndex]);
            }
            else if(localType == text){
                textToDraw.push_back((ztext*)(*containListTmp)[containsListTmpIndex]);
            }
            else if(localType == ellipse){
                paintEllipse(painter, (zEllipse*)(*containListTmp)[containsListTmpIndex]);
            }
            else if(localType == twinLine){
                paintTwinLines(painter, (ztwinline*)(*containListTmp)[containsListTmpIndex]);
            }
            else if(localType == image){
                paintImage(painter, (zImage*)(*containListTmp)[containsListTmpIndex]);
            }
        }
        paintText(painter, &textToDraw, (zellipse->getWidth()) , (zellipse->getHeight()),ellipse);
    }



}
/**
    Paints the twin lines of the ZGNode.\n
    @param painter The painter of the zGNode which paints the twin line.
    @param twinline The twin line to be painted.

*/
void zGNode::paintTwinLines(QPainter *painter,ztwinline* twinline){

     qreal width =0 , height =0;
     width =  qMax(twinline->getXe(),twinline->getX2e()) - qMin(twinline->getX(),twinline->getX2());
     height = qMax(twinline->getYe(),twinline->getY2e()) - qMin(twinline->getY(),twinline->getY2());

     if(lod >= twinline->getZoomStart() && lod < twinline->getZoomEnd()){

         int xM = qMin(twinline->getX(),twinline->getX2());
         int yM = qMin(twinline->getY(),twinline->getY2());


         painter->drawRect(xM,yM,width,height);

         QPen oldPen  = painter->pen();

         QPen tempPen;
         tempPen.setWidth(5);
         painter->setPen(tempPen);

         painter->drawLine(twinline->getX(),twinline->getY(),twinline->getXe(),twinline->getYe());
         painter->drawLine(twinline->getX2(),twinline->getY2(),twinline->getX2e(),twinline->getY2e());

         painter->setPen(oldPen);



         maxX = qMax(twinline->getXe(),twinline->getX2e());
         maxY = qMax(twinline->getYe(),twinline->getY2e());

         minX = qMin(twinline->getX(),twinline->getX2());
         minY = qMin(twinline->getY(),twinline->getY2());
     }

     if(!twinline->getContainsList()->empty()){

         QList<zPrimitive*> *containListTmp = twinline->getContainsList();
         QVector<ztext*> textToDraw;

         for(int containsListTmpIndex = 0; containsListTmpIndex < containListTmp->size(); containsListTmpIndex++){
             int localType = (*containListTmp)[containsListTmpIndex]->getType();
             //TODO OTHERS
             if(localType == rectangle){
                 paintRectangle(painter, (zRectangle*)(*containListTmp)[containsListTmpIndex]);
             }
             else if(localType == text){
                 textToDraw.push_back((ztext*)(*containListTmp)[containsListTmpIndex]);
             }
             else if(localType == ellipse){
                 paintEllipse(painter, (zEllipse*)(*containListTmp)[containsListTmpIndex]);
             }
             else if(localType == twinLine){
                 paintTwinLines(painter, (ztwinline*)(*containListTmp)[containsListTmpIndex]);
             }
             else if(localType == image){
                 paintImage(painter, (zImage*)(*containListTmp)[containsListTmpIndex]);
             }
         }
         paintText(painter, &textToDraw,width,height);
     }
 }

/**
    Finds the anchor points of the ZGNode.\n
    Four anchor points are set. One top, one bottom, one left and one right.
    @param anchor The point to ve evalueated as anchor point.
    @param direction The direction of the point.
    @param deltaS
    @param deltaE

*/
QPointF zGNode::findAnchor(QPointF anchor, int direction, qreal deltaS, qreal deltaE){

    if( ((qAbs((deltaS-deltaE))) <= 0.00001) ){
        if(direction == 1)
            anchor.setX(anchor.x()+((deltaS+deltaE)/2));
        else if(direction == 2)
            anchor.setX(anchor.x()-((deltaS+deltaE)/2));
        else if(direction == 3)
            anchor.setY(anchor.y()+((deltaS+deltaE)/2));
        else
            anchor.setY(anchor.y()-((deltaS+deltaE)/2));

        return anchor;
    }


    if(direction == 1){
        anchor.setX(anchor.x()+ ((deltaS+deltaE)/2) );
        if( !this->contains(mapFromScene(anchor))){
            anchor.setX(anchor.x()- ((deltaS+deltaE)/2) );
            anchor = findAnchor(anchor,1,((deltaS+deltaE)/2), deltaE);
        }else {
            anchor.setX(anchor.x()- ((deltaS+deltaE)/2) );
            anchor = findAnchor(anchor,1, deltaS,((deltaS+deltaE)/2));
        }
    }else if(direction == 2){
        anchor.setX(anchor.x()- ((deltaS+deltaE)/2) );
        if( !this->contains(mapFromScene(anchor))){
            anchor.setX(anchor.x()+ ((deltaS+deltaE)/2) );
            anchor = findAnchor(anchor,2,((deltaS+deltaE)/2), deltaE);
        }else {
            anchor.setX(anchor.x()+ ((deltaS+deltaE)/2) );
            anchor = findAnchor(anchor,2, deltaS,((deltaS+deltaE)/2));
        }
    }else if(direction == 3){
        anchor.setY(anchor.y()+ ((deltaS+deltaE)/2) );
        if( !this->contains(mapFromScene(anchor))){
            anchor.setY(anchor.y()- ((deltaS+deltaE)/2) );
            anchor = findAnchor(anchor,3,((deltaS+deltaE)/2), deltaE);
        }else {
            anchor.setY(anchor.y()- ((deltaS+deltaE)/2) );
            anchor = findAnchor(anchor,3, deltaS,((deltaS+deltaE)/2));
        }
    }else{
        anchor.setY(anchor.y()- ((deltaS+deltaE)/2) );
        if( !this->contains(mapFromScene(anchor))){
            anchor.setY(anchor.y()+ ((deltaS+deltaE)/2) );
            anchor = findAnchor(anchor,4,((deltaS+deltaE)/2), deltaE);
        }else {
            anchor.setY(anchor.y()+ ((deltaS+deltaE)/2) );
            anchor = findAnchor(anchor,4, deltaS,((deltaS+deltaE)/2));
        }
    }

    return anchor;
}
/**
    Paints the ZGNode.\n
    Finds anchor points and sets animations.
    @param painter The painter to draw the zGNode.

*/
void zGNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *){

    maxXEllipse = false;
    maxYEllipse = false;
    drawPainter = false;
    this->lod = option->levelOfDetailFromTransform(painter->worldTransform());

    //NODE COLOR
    QColor *color2 = new QColor(QString(this->getNode()->getColor()));
    QColor *color = new QColor(QString(this->getNode()->getFillColor()));

    if(this->getNode()->getColor() == "transparent"){
        color->setAlpha(0);
    }
    if(this->getNode()->getFillColor() == ""){
        color = color2;
    }
    QBrush brush(*color);
    painter->setBrush(brush);

    QPointF anchorPoint(-1,-1);
    for(int arrayIndex = 0; arrayIndex < 4; arrayIndex++ ){
        anchorPointArray[arrayIndex] = anchorPoint;
    }

    QPen pen(*color2);
    pen.setCosmetic(true);
    pen.setWidth(1);
    painter->setPen(pen);


    if(lod >= minLod + 0.1 && lod <= maxLod && !firstPaint ){

            animation->setStartValue(0);
            animation->setEndValue(1);
            animation->setEasingCurve(QEasingCurve::Linear);
            animation->stop();
            animation->start();
            firstPaint =true;

    }
    else if(lod <= minLod + 0.1 && firstPaint){

        if(fatherId != 0){
            firstPaint = false;
            animation->setStartValue(1);
            animation->setEndValue(0.001);
            animation->setEasingCurve(QEasingCurve::Linear);
            animation->stop();
            animation->start();
        }

    }

    if(lod <= this->node->getCustomZoom() + 0.15 && lod >= this->node->getCustomZoom() && this->node->getCustomLayout() != "null" ){

            if(this->node->getCustomLayout() == "matrix"){
                if(trigger == "null"){
                    trigger = "matrix";
                }

            }
            else if(this->node->getCustomLayout() == "circle"){
                if(trigger == "null"){
                    trigger = "circle";
                }

            }
    }



    QList<zPrimitive*> *nodePrimitiveList = this->node->getElementsList();
    QVector<ztext*> textToDraw;

    maxX = 0;
    maxY = 0;
    minX = 99999;
    minY = 99999;

    for(int nodeListIndex = 0; nodeListIndex < nodePrimitiveList->size(); nodeListIndex++){
        int localType = (*nodePrimitiveList)[nodeListIndex]->getType();
        //TODO Others
        if(localType == rectangle){
            paintRectangle(painter,(zRectangle*)(*nodePrimitiveList)[nodeListIndex]);
        }
        else if(localType == text){
            textToDraw.push_back((ztext*)(*nodePrimitiveList)[nodeListIndex]);
        }
        else if(localType == ellipse){
            paintEllipse(painter,(zEllipse*)(*nodePrimitiveList)[nodeListIndex]);
        }
        else if(localType == twinLine){
            paintTwinLines(painter,(ztwinline*)(*nodePrimitiveList)[nodeListIndex]);
        }
        else if(localType == image){
            paintImage(painter, (zImage*)(*nodePrimitiveList)[nodeListIndex]);
        }
    }

     paintText(painter, &textToDraw, -1,-1);

    setData(6,   this->getNodeXPos() + minX*scale() + ((maxX*scale()/2.0f)));
    setData(7,   this->getNodeYPos() + minY*scale() + ((maxY*scale()/2.0f)));

    QPointF middlePoint(data(6).toReal(),data(7).toReal());
    QPointF anchorPointLeft = middlePoint;
    QPointF anchorPointRight = middlePoint;
    QPointF anchorPointUp = middlePoint;
    QPointF anchorPointDown = middlePoint;
    float value = 1;

    while(anchorPointArray[0] == anchorPoint || anchorPointArray[1] == anchorPoint
          || anchorPointArray[2] == anchorPoint || anchorPointArray[3] == anchorPoint){

        if(anchorPointArray[0] == anchorPoint){
            if(this->contains(mapFromScene(anchorPointLeft))){

                anchorPointLeft.setX(anchorPointLeft.x()-value);
            }else{
                anchorPointLeft = findAnchor(anchorPointLeft,1,0,value);
                anchorPointArray[0] = anchorPointLeft;
            }
        }
        if(anchorPointArray[1] == anchorPoint){
            if(this->contains(mapFromScene(anchorPointRight))){
                anchorPointRight.setX(anchorPointRight.x()+value);
            }else{
                anchorPointRight = findAnchor(anchorPointRight,2,0,value);
                anchorPointArray[1] = anchorPointRight;
            }
        }
        if(anchorPointArray[2] == anchorPoint){
            if(this->contains(mapFromScene(anchorPointUp))){

                anchorPointUp.setY(anchorPointUp.y()-value);
            }else{
                anchorPointUp = findAnchor(anchorPointUp,3,0,value);
                anchorPointArray[2] = anchorPointUp;
            }
        }
        if(anchorPointArray[3] == anchorPoint){
            if(this->contains(mapFromScene(anchorPointDown))){

                anchorPointDown.setY(anchorPointDown.y()+value);
            }else{
                anchorPointDown = findAnchor(anchorPointDown,4,0,value);
                anchorPointArray[3] = anchorPointDown;
            }
        }
    }

    lastLod = lod;
    delete color;
    delete color2;

    return;
}

void zGNode::setNodeScaleFactor(qreal nodeScaleFactor){
    this->nodeScaleFactor = nodeScaleFactor;
}

qreal zGNode::getNodeScaleFactor(){
    return this->nodeScaleFactor;
}

void zGNode::setFatherId(int fatherId){
    this->fatherId = fatherId;
}

int zGNode::getFatherId(){
    return this->fatherId;
}
QPointF* zGNode::getAnchorPointArray(){
    return this->anchorPointArray;
}

zNode* zGNode::getNode(){
    return this->node;
}
qreal zGNode::getLod(){
    return this->lod;
}

qreal zGNode::getNodeXPos(){
    return this->nodeXPos;
}

qreal zGNode::getNodeYPos(){
    return this->nodeYPos;
}

void zGNode::setNodeXPos(qreal nodeXPos){
    this->nodeXPos=nodeXPos;
}

void zGNode::setNodeYPos(qreal nodeYPos){
    this->nodeYPos=nodeYPos;
}

qreal zGNode::getMaxX(){
    return maxX;
}

qreal zGNode::getMaxY(){
    return maxY;
}

qreal zGNode::getFatherScaleVal(){
    return fatherScaleVal;
}

QString zGNode::getTrigger(){
    return this->trigger;
}

void zGNode::setTrigger(QString trigger){
    this->trigger = trigger;
}

/**
    Sets the animation properties of the ZGNode.\n

    @param startValueX The current x coordinate of the zGNode.
    @param startValueY The current y coordinate of the zGNode.
    @param endValueY The final x coordinate of the zGnode.
    @param endValueY The final y coordinate of the zGnode.

*/
void zGNode::setAnimationFinalProperties(qreal startValueX, qreal endValueX, qreal startValueY, qreal endValueY){

    QPointF startPos, endPos;
    startPos.setX(startValueX);
    startPos.setY(startValueY);

    endPos.setX(endValueX);
    endPos.setY(endValueY);

    animationFinalX->setStartValue(startPos);
    animationFinalX->setEndValue(endPos);

}

void zGNode::startAnimations(){
    animationFinalX->start();
}




