#ifndef ZCLASS_H
#define ZCLASS_H

#include <QGraphicsObject>
#include <QString>
#include "../znode.h"
#include <QtGlobal>
#include <QGraphicsOpacityEffect>
#include <QPropertyAnimation>
#include <qvector.h>
#include<QPixmap>

enum primivitesShapes {rectangle = 1, ellipse = 2, text = 3, twinLine = 4, image = 5};



class zGNode : public QGraphicsObject
{

public:
    zGNode();
    zGNode(zNode* node, int width, int height, qreal fatherScale, qreal fatherXPos, qreal fatherYPos);
    ~zGNode();
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                   QWidget *widget) Q_DECL_OVERRIDE;

    void paintRectangle(QPainter *painter, zRectangle* rectangle);
    void paintEllipse(QPainter *painter, zEllipse* ellipse);
    void paintText(QPainter *painter, QVector<ztext*> *textVector, qreal widht, qreal height, int type);
    void paintTwinLines(QPainter *painter, ztwinline* twinline);
    void paintImage(QPainter *painter, zImage* zimage);
    void setNodeScaleFactor(qreal nodeScaleFactor);
    qreal getNodeScaleFactor();
    void setFatherId(int fatherId);
    int getFatherId();
    QPointF* getAnchorPointArray();
    zNode* getNode();
    qreal getLod();

    void pathRectangle(QPainterPath* path, zRectangle* rectangle) const;
    void pathEllipse(QPainterPath* path, zEllipse* ellipse) const;
    void pathTwinLine(QPainterPath * path, ztwinline* twinline) const;
    void pathImage(QPainterPath* path, zImage* image) const;

    qreal getNodeXPos();
    qreal getNodeYPos();
    qreal getFatherScaleVal();
    void setNodeXPos(qreal nodeXPos);
    void setNodeYPos(qreal nodeYPos);

    qreal getMaxX();
    qreal getMaxY();

    void setAnimationFinalProperties(qreal startValueX, qreal endValueX, qreal startValueY, qreal endValueY);
    void startAnimations();

    QString getTrigger();
    void setTrigger(QString trigger);

private:

    zNode* node;

    qreal maxX;
    qreal maxY;
    qreal minX;
    qreal minY;

    qreal centerX;
    qreal centerY;
    bool maxXEllipse;
    bool maxYEllipse;

    qreal nodeXPos;
    qreal nodeYPos;
    qreal nodeScaleFactor;
    int fatherId;
    qreal fatherScaleVal;


    QList <QPair <QPointF,QPointF> > ellipses;

    QPointF anchorPointArray[4];
    qreal lod;
    qreal lastLod;
    QPainterPath test;
    QGraphicsOpacityEffect  *opacity;

    QPointF findAnchor(QPointF anchor, int direction, qreal deltaS, qreal deltaE);
    qreal minLod;
    qreal maxLod;
    bool firstPaint;
    bool actuallyDrawn;
    bool drawPainter;

    QPropertyAnimation *animation;
    QPropertyAnimation *animationFinalX;

    QString trigger;

};

#endif // ZCLASS_H
