#ifndef ZPRIMITIVE_H
#define ZPRIMITIVE_H
#include <QList>
#include <QDebug>

class zPrimitive
{
public:

    zPrimitive(){
        this->containsList = new QList<zPrimitive*>();
    }

    ~zPrimitive(){
        delete containsList;
    }

    void setX(int x){
        this->x = x;
    }
    void setY(int y){
        this->y = y;
    }
    void setZoomStart(double zoomStart){
        this->zoomStart = zoomStart;
    }
    void setZoomEnd(double zoomEnd){
        this->zoomEnd = zoomEnd;
    }
    int getX(){
        return x;
    }
    int getY(){
        return y;
    }
    double getZoomStart(){
        return zoomStart;
    }
    double getZoomEnd(){
        return zoomEnd;
    }
    QList<zPrimitive*> *getContainsList(){
        return containsList;
    }
    void setContainsList(QList<zPrimitive*> * containsList){
        this->containsList = containsList;
    }
    int getType(){
        return type;
    }
    void setType(int type){
        this->type = type;
    }

    QString getColor(){
        return color;
    }

    void setColor(QString color){
        this->color = color;
    }

protected:
    int x;
    int y;
    double zoomStart;
    double zoomEnd;
    int type;
    QString color;
    QList<zPrimitive*> *containsList;

};


#endif // ZPRIMITIVE_H
