#ifndef ZRECTANGLE_H
#define ZRECTANGLE_H
#include "zprimitive.h"


class zRectangle: public zPrimitive
{
public:
    zRectangle();
    ~zRectangle();
    void setWidth(double width);
    void setHeight(double height);
    void setRadius(qreal radius);
    void setCurveTopLeft(bool curveTopLeft);
    void setCurveTopRight(bool curveTopRight);
    void setCurveBottomLeft(bool curveBottomLeft);
    void setCurveBottomRight(bool curveBottomRight);

    double getWidth();
    double getHeight();
    qreal getRadius();
    bool getCurveTopLeft();
    bool getCurveTopRight();
    bool getCurveBottomLeft();
    bool getCurveBottomRight();

private:
    double width;
    double height;
    qreal radius;
    bool curveTopLeft;
    bool curveTopRight;
    bool curveBottomLeft;
    bool curveBottomRight;

};

#endif // ZRECTANGLE_H
