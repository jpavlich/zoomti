#include "ztwinline.h"

ztwinline::ztwinline()
{

}

ztwinline::~ztwinline()
{

}

void ztwinline::setXe(int xe){
    this->xe = xe;
}

void ztwinline::setYe(int ye){
    this->ye = ye;
}

void ztwinline::setX2(int x2){
    this->x2 = x2;
}

void ztwinline::setY2(int y2){
    this->y2 = y2;
}

void ztwinline::setX2e(int x2e){
    this->x2e = x2e;
}

void ztwinline::setY2e(int y2e){
    this->y2e = y2e;
}

int ztwinline::getXe(){
    return xe;
}

int ztwinline::getYe(){
    return ye;
}

int ztwinline::getX2(){
    return x2;
}

int ztwinline::getY2(){
    return y2;
}

int ztwinline::getX2e(){
    return x2e;
}

int ztwinline::getY2e(){
    return y2e;
}


