#ifndef ZTWINLINE_H
#define ZTWINLINE_H
#include "zprimitive.h"

class ztwinline: public zPrimitive
{
public:
    ztwinline();
    ~ztwinline();

    void setXe(int xe);
    void setYe(int ye);
    void setX2(int x2);
    void setY2(int y2);
    void setX2e(int x2e);
    void setY2e(int y2e);

    int getXe();
    int getYe();
    int getX2();
    int getY2();
    int getX2e();
    int getY2e();

private:
    int xe;
    int ye;
    int x2;
    int y2;
    int x2e;
    int y2e;
};

#endif // ZTWINLINE_H
