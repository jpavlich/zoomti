# README #

ZoomTI++: Framework to visualize software diagrams by using Zoomable User Interfaces and Multi-Touch technology.

---------------------------------------------------

Website with full information about the framework (Spanish): [http://pegasus.javeriana.edu.co/~CIS1510AP01/](http://pegasus.javeriana.edu.co/~CIS1510AP01/).

Example video (English): [https://www.youtube.com/watch?v=RzwmzY4Zelk](https://www.youtube.com/watch?v=RzwmzY4Zelk)

Example video (Spanish): [https://www.youtube.com/watch?v=eBo0PRWQQcg](https://www.youtube.com/watch?v=eBo0PRWQQcg)

---------------------------------------------------

Created on 2015 using QT by Eduardo Montenegro ([eduardo116](https://bitbucket.org/eduardo116/)) and Daniel Rico ([danielricoh](https://bitbucket.org/danielricoh/)), under the direction of Ph.D Jaime Pavlich ([jpavlich](https://bitbucket.org/jpavlich/)).