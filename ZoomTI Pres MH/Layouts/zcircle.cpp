#include "zcircle.h"
#include "math.h"

zcircle::zcircle()
{

}

zcircle::~zcircle()
{

}
/**
    Executes the circle layout .\n
    Reorganizes the zGEdges shown in the scene.
*/
void zcircle::executeLayout(){

    qreal columnWidth = this->containerWidth;
    qreal rowHeight = this->containerHeight;

    int angleIndex = 0;
    int nodeNumber = 0;

    qreal maxHeight = 0;
    qreal maxWidth = 0;

    for( QMap<int, zGNode*>::iterator mapIt = graphicNodeMap.begin(); mapIt !=graphicNodeMap.end(); mapIt++)
        if( mapIt.value()->getFatherId() == fatherId){
            nodeNumber++;
            maxHeight = qMax(maxHeight, mapIt.value()->getMaxY()*mapIt.value()->scale());
            maxWidth = qMax(maxWidth, mapIt.value()->getMaxX()*mapIt.value()->scale());
        }

    qreal centerX = containerOriginPoint.x() + columnWidth / 2;
    qreal centerY = containerOriginPoint.y() + rowHeight / 2;



    int nodeAngle = 360/nodeNumber;

    for( QMap<int, zGNode*>::iterator mapIt = graphicNodeMap.begin(); mapIt !=graphicNodeMap.end(); mapIt++){
        if( mapIt.value()->getFatherId() == fatherId){

            qreal targetX;
            qreal targetY;

            qreal nodeHeight = mapIt.value()->getMaxY()*mapIt.value()->scale();
            qreal nodeWidth = mapIt.value()->getMaxX()*mapIt.value()->scale();

            qreal distanceX = qAbs((columnWidth / 2 - nodeWidth/2))*0.9;
            qreal distanceY = qAbs((rowHeight / 2 - nodeHeight/2)) *0.9;

            targetY = ( centerY + distanceY * ::sinf( nodeAngle * angleIndex * 3.1415926 / 180 ) ) -  nodeHeight/2;
            targetX = ( centerX + distanceX * ::cosf( nodeAngle * angleIndex * 3.1415926 / 180 ) ) -  nodeWidth/2;



            angleIndex++;

            targetX -= mapIt.value()->getNodeXPos();
            targetY -= mapIt.value()->getNodeYPos();

            translateNode(mapIt.value()->getNode()->getNId(), targetX, targetY);
        }
    }
}

/**
    Executes the circle layout without animation.\n
    Reorganizes the zGEdges shown in the scene.
*/
void zcircle::executeLayoutNoAnimation(){

    qreal columnWidth = this->containerWidth;
    qreal rowHeight = this->containerHeight;

    int angleIndex = 0;
    int nodeNumber = 0;

    qreal maxHeight = 0;
    qreal maxWidth = 0;

    for( QMap<int, zGNode*>::iterator mapIt = graphicNodeMap.begin(); mapIt !=graphicNodeMap.end(); mapIt++)
        if( mapIt.value()->getFatherId() == fatherId){
            nodeNumber++;
            maxHeight = qMax(maxHeight, mapIt.value()->getMaxY()*mapIt.value()->scale());
            maxWidth = qMax(maxWidth, mapIt.value()->getMaxX()*mapIt.value()->scale());
        }

    qreal centerX = containerOriginPoint.x() + columnWidth / 2;
    qreal centerY = containerOriginPoint.y() + rowHeight / 2;

    int nodeAngle = 360/nodeNumber;

    for( QMap<int, zGNode*>::iterator mapIt = graphicNodeMap.begin(); mapIt !=graphicNodeMap.end(); mapIt++){
        if( mapIt.value()->getFatherId() == fatherId){

            qreal targetX;
            qreal targetY;

            qreal nodeHeight = mapIt.value()->getMaxY()*mapIt.value()->scale();
            qreal nodeWidth = mapIt.value()->getMaxX()*mapIt.value()->scale();

            qreal distanceX = qAbs((columnWidth / 2 - nodeWidth/2))*0.9;
            qreal distanceY = qAbs((rowHeight / 2 - nodeHeight/2)) *0.9;


            targetY = ( centerY + distanceY * ::sinf( nodeAngle * angleIndex * 3.1415926 / 180 ) ) -  nodeHeight/2;
            targetX = ( centerX + distanceX * ::cosf( nodeAngle * angleIndex * 3.1415926 / 180 ) ) -  nodeWidth/2;

            angleIndex++;

            targetX -= mapIt.value()->getNodeXPos();
            targetY -= mapIt.value()->getNodeYPos();

            translateNodeNoAnimation(mapIt.value()->getNode()->getNId(), targetX, targetY);
        }
    }
}
