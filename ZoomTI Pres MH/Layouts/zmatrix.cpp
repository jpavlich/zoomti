#include "zmatrix.h"

/**
    zMatrix Constructor \n
    Sets the initial matrix where the nodes will be set.
*/
zmatrix::zmatrix(int rowNumber, int columnNumber)
{
    this->rowNumber = 3;
    this->columnNumber = 4;
    this->autoResize = false;
}

zmatrix::~zmatrix()
{

}

void zmatrix::setAutoResize(bool autoResize){
    this->autoResize = autoResize;
}

bool zmatrix::getAutoResize(){
    return this->autoResize;
}

void zmatrix::setColumnNumber(int columnNumber){
    this->columnNumber = columnNumber;
}

void zmatrix::setRowNumber(int rowNumber){
    this->rowNumber = rowNumber;
}

/**
    Executes the matrix layout .\n
    Reorganizes the zGEdges shown in the scene into the matrix.
*/
void zmatrix::executeLayout(){
    autoResize = true;
    int columnWidth = this->containerWidth / this->columnNumber;
    int rowHeight = this->containerHeight / this->rowNumber;

    int columnIndex = 0;
    int rowIndex = 0;

    qreal lastX= containerOriginPoint.x(), lastY= containerOriginPoint.y();
    qreal maxHeight = 0;

    for( QMap<int, zGNode*>::iterator mapIt = graphicNodeMap.begin(); mapIt !=graphicNodeMap.end(); mapIt++){
        if( mapIt.value()->getFatherId() == fatherId){
            qreal targetX;
            qreal targetY;

            if(autoResize){
                targetX = lastX;
                targetY = lastY;
            }
            else{
                targetX = containerOriginPoint.x() +  columnIndex * columnWidth;
                targetY = containerOriginPoint.y() +  rowIndex * rowHeight;
            }

            columnIndex++;

            lastX = targetX + mapIt.value()->getMaxX()*mapIt.value()->scale() + this->containerWidth*0.05;
            maxHeight = qMax(maxHeight, mapIt.value()->getMaxY()*mapIt.value()->scale());

            if(columnIndex % columnNumber == 0){

                columnIndex = 0;
                lastX = containerOriginPoint.x();
                rowIndex++;
                lastY = targetY + maxHeight + this->containerHeight*0.05;

                if(rowIndex % rowNumber == 0){
                    rowIndex = 0;
                    lastY = containerOriginPoint.y();
                }
            }

            targetX -= mapIt.value()->getNodeXPos();
            targetY -= mapIt.value()->getNodeYPos();

            translateNode(mapIt.value()->getNode()->getNId(), targetX, targetY);
        }
    }
}

/**
    Executes the matrix layout without animation .\n
    Reorganizes the zGEdges shown in the scene into the matrix.
*/
void zmatrix::executeLayoutNoAnimation(){

    int columnWidth = this->containerWidth / this->columnNumber;
    int rowHeight = this->containerHeight / this->rowNumber;

    int columnIndex = 0;
    int rowIndex = 0;

    qreal lastX= containerOriginPoint.x(), lastY= containerOriginPoint.y();
    qreal maxHeight = 0;

    for( QMap<int, zGNode*>::iterator mapIt = graphicNodeMap.begin(); mapIt !=graphicNodeMap.end(); mapIt++){
        if( mapIt.value()->getFatherId() == fatherId){
            qreal targetX;
            qreal targetY;

            if(autoResize){
                targetX = lastX;
                targetY = lastY;
            }
            else{
                targetX = containerOriginPoint.x() +  columnIndex * columnWidth;
                targetY = containerOriginPoint.y() +  rowIndex * rowHeight;
            }

            columnIndex++;

            lastX = targetX + mapIt.value()->getMaxX()*mapIt.value()->scale() + this->containerWidth*0.05;
            maxHeight = qMax(maxHeight, mapIt.value()->getMaxY()*mapIt.value()->scale());

            if(columnIndex % columnNumber == 0){

                columnIndex = 0;
                lastX = containerOriginPoint.x();
                rowIndex++;
                lastY = targetY + maxHeight + this->containerHeight*0.05;

                if(rowIndex % rowNumber == 0){
                    rowIndex = 0;
                    lastY = containerOriginPoint.y();
                }
            }

            targetX -= mapIt.value()->getNodeXPos();
            targetY -= mapIt.value()->getNodeYPos();

            translateNodeNoAnimation(mapIt.value()->getNode()->getNId(), targetX, targetY);
        }
    }
}

