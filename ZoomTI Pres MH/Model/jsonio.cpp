#include "jsonio.h"
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>

/**
    JsonIO Constructor \n
    Opens the JSON file specified, and calls the read() function.
    @param filePath the string that contains the path of the JSON document.
*/
JsonIO::JsonIO(QString filePath)
{
    QFile loadFile( filePath);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    jsonObject = loadDoc.object();
    read();



}
/**
    Saves a ellipse array specified in a JSON document into a zPrimitive array. \n
    @param ellipseArray The array that contains the ellipses specified in the JSON document.
    @param primitiveToAdd The listo of primitives where the ellipses are going to be added.
    @param fatherRelPosition The relative position of the container.
    @param fatherAbsPosition The absolute position of the container.
    @param fatherId The id of the container (zGNode).
*/
void JsonIO::readEllipse(QJsonArray ellipseArray, QList<zPrimitive*> *primitiveToAdd, QPoint fatherRelPosition, QPoint fatherAbsPosition, int fatherId)
{
    for( int ellipseIndex = 0; ellipseIndex < ellipseArray.size(); ++ellipseIndex) {

        zEllipse* ellipseToAddToList = new zEllipse();

        QJsonObject ellipseObject = ellipseArray[ellipseIndex].toObject();

        ellipseToAddToList->setX(ellipseObject["x"].toInt() + fatherRelPosition.x());
        ellipseToAddToList->setY(ellipseObject["y"].toInt() + fatherRelPosition.y());
        ellipseToAddToList->setZoomStart(ellipseObject["zoomStart"].toDouble());
        ellipseToAddToList->setZoomEnd(ellipseObject["zoomEnd"].toDouble());
        ellipseToAddToList->setWidth(ellipseObject["width"].toDouble());
        ellipseToAddToList->setHeight(ellipseObject["height"].toDouble());
        ellipseToAddToList->setType(ellipseObject["type"].toInt());
        ellipseToAddToList->setColor(ellipseObject["color"].toString());

        //Relative position of this element to pass to his childrens.
        QPoint nFatherRelPosition(ellipseToAddToList->getX(), ellipseToAddToList->getY());

        QJsonObject contains = ellipseObject["contains"].toObject();
        if(!contains.empty()){
            for(QJsonObject::iterator it = contains.begin(); it != contains.end(); it++){
                if(it.key() == "rectangle"){
                    QJsonArray containsRectangle = it.value().toArray();
                    readRectangle(containsRectangle,(ellipseToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
                else if(it.key() == "node"){
                    QJsonArray containsNode = it.value().toArray();
                    readNode(containsNode, nFatherRelPosition, fatherAbsPosition, fatherId,ellipseToAddToList->getWidth(), ellipseToAddToList->getHeight());
                }
                else if(it.key() == "text"){
                    QJsonArray containsText = it.value().toArray();
                    readText(containsText,(ellipseToAddToList->getContainsList()),nFatherRelPosition);
                }
                else if(it.key() == "ellipse"){
                    QJsonArray containsEllipse = it.value().toArray();
                    readEllipse(containsEllipse,(ellipseToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
                else if(it.key() == "twinLines"){
                    QJsonArray containsTwinLine = it.value().toArray();
                    readTwinLines(containsTwinLine,(ellipseToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
                else if(it.key() == "image"){
                    QJsonArray containsImage = it.value().toArray();
                    readImage(containsImage,(ellipseToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
            }

        }

        primitiveToAdd->push_back((zPrimitive*)ellipseToAddToList);
    }
}
/**
    Saves a text array specified in a JSON document into a zPrimitive array. \n
    @param textArray The array that contains the texts specified in the JSON document.
    @param primitiveToAdd The listo of primitives where the texts are going to be added.
    @param fatherRelPosition The relative position of the container.
    @param fatherAbsPosition The absolute position of the container.
    @param fatherId The id of the container (zGNode).
*/
void JsonIO::readText(QJsonArray textArray, QList<zPrimitive*> *primitiveToAdd, QPoint fatherRelPosition)
{
    for( int textIndex = 0; textIndex < textArray.size(); textIndex++){
        ztext* textToAddToList = new ztext();

        QJsonObject textObject = textArray[textIndex].toObject();

        textToAddToList->setX(textObject["x"].toInt() + fatherRelPosition.x());
        textToAddToList->setY(textObject["y"].toInt() + fatherRelPosition.y());
        textToAddToList->setZoomStart(textObject["zoomStart"].toDouble());
        textToAddToList->setZoomEnd(textObject["zoomEnd"].toDouble());
        textToAddToList->setType(textObject["type"].toInt());
        textToAddToList->setAlign(textObject["align"].toString());
        textToAddToList->setText(textObject["text"].toString());

        primitiveToAdd->push_back((zPrimitive*)textToAddToList);
    }

}
/**
    Saves a rectangle array specified in a JSON document into a zPrimitive array. \n
    @param rectangleArray The array that contains the rectangles specified in the JSON document.
    @param primitiveToAdd The listo of primitives where the rectangles are going to be added.
    @param fatherRelPosition The relative position of the container.
    @param fatherAbsPosition The absolute position of the container.
    @param fatherId The id of the container (zGNode).
*/
void JsonIO::readRectangle(QJsonArray rectangleArray, QList<zPrimitive*> *primitiveToAdd, QPoint fatherRelPosition, QPoint fatherAbsPosition, int fatherId)
{
    for( int rectangleIndex = 0; rectangleIndex < rectangleArray.size(); ++rectangleIndex) {

        zRectangle* rectangleToAddToList = new zRectangle();

        QJsonObject rectangleObject = rectangleArray[rectangleIndex].toObject();

        rectangleToAddToList->setX(rectangleObject["x"].toInt() + fatherRelPosition.x());
        rectangleToAddToList->setY(rectangleObject["y"].toInt() + fatherRelPosition.y());
        rectangleToAddToList->setZoomStart(rectangleObject["zoomStart"].toDouble());
        rectangleToAddToList->setZoomEnd(rectangleObject["zoomEnd"].toDouble());
        rectangleToAddToList->setWidth(rectangleObject["width"].toDouble());
        rectangleToAddToList->setHeight(rectangleObject["height"].toDouble());
        rectangleToAddToList->setType(rectangleObject["type"].toInt());
        rectangleToAddToList->setRadius(rectangleObject["radius"].toDouble());

        rectangleToAddToList->setCurveTopLeft(rectangleObject["curveTopLeft"].toInt());
        rectangleToAddToList->setCurveTopRight(rectangleObject["curveTopRight"].toInt());
        rectangleToAddToList->setCurveBottomLeft(rectangleObject["curveBottomLeft"].toInt());
        rectangleToAddToList->setCurveBottomRight(rectangleObject["curveBottomRight"].toInt());
        rectangleToAddToList->setColor(rectangleObject["color"].toString());

        //Relative position of this element to pass to his childrens.
        QPoint nFatherRelPosition(rectangleToAddToList->getX(), rectangleToAddToList->getY());

        QJsonObject contains = rectangleObject["contains"].toObject();
        if(!contains.empty()){
            for(QJsonObject::iterator it = contains.begin(); it != contains.end(); it++){
                if(it.key() == "rectangle"){
                    QJsonArray containsRectangle = it.value().toArray();
                    readRectangle(containsRectangle,(rectangleToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
                else if(it.key() == "node"){
                    QJsonArray containsNode = it.value().toArray();
                    readNode(containsNode, nFatherRelPosition, fatherAbsPosition, fatherId,rectangleToAddToList->getWidth(), rectangleToAddToList->getHeight());
                }
                else if(it.key() == "text"){
                    QJsonArray containsText = it.value().toArray();
                    readText(containsText,(rectangleToAddToList->getContainsList()),nFatherRelPosition);
                }
                else if(it.key() == "twinLines"){
                    QJsonArray containsTwinLine = it.value().toArray();
                    readTwinLines(containsTwinLine,(rectangleToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                } else if(it.key() == "ellipse"){
                    QJsonArray containsEllipse = it.value().toArray();
                    readEllipse(containsEllipse,(rectangleToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                } else if(it.key() == "image"){
                    QJsonArray containsImage = it.value().toArray();
                    readImage(containsImage,(rectangleToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
            }
        }

        primitiveToAdd->push_back((zPrimitive*)rectangleToAddToList);
    }
}


/**
    Saves a image array specified in a JSON document into a zPrimitive array. \n
    @param imageArray The array that contains the images specified in the JSON document.
    @param primitiveToAdd The listo of primitives where the images are going to be added.
    @param fatherRelPosition The relative position of the container.
    @param fatherAbsPosition The absolute position of the container.
    @param fatherId The id of the container (zGNode).
*/
void JsonIO::readImage(QJsonArray imageArray, QList<zPrimitive*> *primitiveToAdd, QPoint fatherRelPosition, QPoint fatherAbsPosition, int fatherId)
{
    for( int imageIndex = 0; imageIndex < imageArray.size(); ++imageIndex) {

        zImage* imageToAddToList = new zImage();

        QJsonObject imageObject = imageArray[imageIndex].toObject();

        imageToAddToList->setX(imageObject["x"].toInt() + fatherRelPosition.x());
        imageToAddToList->setY(imageObject["y"].toInt() + fatherRelPosition.y());
        imageToAddToList->setZoomStart(imageObject["zoomStart"].toDouble());
        imageToAddToList->setZoomEnd(imageObject["zoomEnd"].toDouble());
        imageToAddToList->setWidth(imageObject["width"].toDouble());
        imageToAddToList->setHeight(imageObject["height"].toDouble());
        imageToAddToList->setType(imageObject["type"].toInt());
        imageToAddToList->setImg(imageObject["image"].toString());

        //Relative position of this element to pass to his childrens.
        QPoint nFatherRelPosition(imageToAddToList->getX(), imageToAddToList->getY());

        QJsonObject contains = imageObject["contains"].toObject();
        if(!contains.empty()){
            for(QJsonObject::iterator it = contains.begin(); it != contains.end(); it++){
                if(it.key() == "rectangle"){
                    QJsonArray containsRectangle = it.value().toArray();
                    readRectangle(containsRectangle,(imageToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
                else if(it.key() == "node"){
                    QJsonArray containsNode = it.value().toArray();
                    readNode(containsNode, nFatherRelPosition, fatherAbsPosition, fatherId,imageToAddToList->getWidth(), imageToAddToList->getHeight());
                }
                else if(it.key() == "text"){
                    QJsonArray containsText = it.value().toArray();
                    readText(containsText,(imageToAddToList->getContainsList()),nFatherRelPosition);
                }
                else if(it.key() == "twinLines"){
                    QJsonArray containsTwinLine = it.value().toArray();
                    readTwinLines(containsTwinLine,(imageToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
                else if(it.key() == "ellipse"){
                    QJsonArray containsEllipse = it.value().toArray();
                    readEllipse(containsEllipse,(imageToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
                else if(it.key() == "image"){
                    QJsonArray containsImage = it.value().toArray();
                    readImage(containsImage,(imageToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
            }

        }

        primitiveToAdd->push_back((zPrimitive*)imageToAddToList);
    }
}
/**
    Saves a TwinLine array specified in a JSON document into a zPrimitive array. \n
    @param twinLinesArray The array that contains the twin lines specified in the JSON document.
    @param primitiveToAdd The listo of primitives where the twin lines are going to be added.
    @param fatherRelPosition The relative position of the container.
    @param fatherAbsPosition The absolute position of the container.
    @param fatherId The id of the container (zGNode).
*/

void JsonIO::readTwinLines(QJsonArray twinLinesArray, QList<zPrimitive*> *primitiveToAdd, QPoint fatherRelPosition, QPoint fatherAbsPosition, int fatherId){

    for( int twinLinesIndex = 0; twinLinesIndex < twinLinesArray.size(); ++twinLinesIndex) {

        ztwinline * twinLineToAddToList = new ztwinline();

        QJsonObject twinLineObject = twinLinesArray[twinLinesIndex].toObject();

        twinLineToAddToList->setX(twinLineObject["x"].toInt() + fatherRelPosition.x());
        twinLineToAddToList->setY(twinLineObject["y"].toInt() + fatherRelPosition.y());
        twinLineToAddToList->setXe(twinLineObject["xe"].toInt() + fatherRelPosition.x());
        twinLineToAddToList->setYe(twinLineObject["ye"].toInt() + fatherRelPosition.y());

        twinLineToAddToList->setX2(twinLineObject["x2"].toInt() + fatherRelPosition.x());
        twinLineToAddToList->setY2(twinLineObject["y2"].toInt() + fatherRelPosition.y());
        twinLineToAddToList->setX2e(twinLineObject["x2e"].toInt() + fatherRelPosition.x());
        twinLineToAddToList->setY2e(twinLineObject["y2e"].toInt() + fatherRelPosition.y());

        twinLineToAddToList->setZoomStart(twinLineObject["zoomStart"].toDouble());
        twinLineToAddToList->setZoomEnd(twinLineObject["zoomEnd"].toDouble());

        twinLineToAddToList->setType(twinLineObject["type"].toInt());

        int width =  qMax(twinLineToAddToList->getXe(),twinLineToAddToList->getX2e()) - qMin(twinLineToAddToList->getX(),twinLineToAddToList->getX2());
        int height = qMax(twinLineToAddToList->getYe(),twinLineToAddToList->getY2e()) - qMin(twinLineToAddToList->getY(),twinLineToAddToList->getY2());

        //Relative position of this element to pass to his childrens.
        QPoint nFatherRelPosition(twinLineToAddToList->getX(), twinLineToAddToList->getY());

        QJsonObject contains = twinLineObject["contains"].toObject();
        if(!contains.empty()){
            for(QJsonObject::iterator it = contains.begin(); it != contains.end(); it++){
                if(it.key() == "rectangle"){
                    QJsonArray containsRectangle = it.value().toArray();
                    readRectangle(containsRectangle,(twinLineToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
                else if(it.key() == "node"){
                    QJsonArray containsNode = it.value().toArray();
                    readNode(containsNode, nFatherRelPosition, fatherAbsPosition, fatherId,width, height);
                }
                else if(it.key() == "text"){
                    QJsonArray containsText = it.value().toArray();
                    readText(containsText,(twinLineToAddToList->getContainsList()),nFatherRelPosition);
                }
                else if(it.key() == "twinLines"){
                    QJsonArray containsTwinLine = it.value().toArray();
                    readTwinLines(containsTwinLine,(twinLineToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }
                else if(it.key() == "ellipse"){
                    QJsonArray containsEllipse = it.value().toArray();
                    if(!containsEllipse.empty()){
                            readRectangle(containsEllipse,(twinLineToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                    }
                }
                else if(it.key() == "image"){
                    QJsonArray containsImage = it.value().toArray();
                    readImage(containsImage,(twinLineToAddToList->getContainsList()),nFatherRelPosition, fatherAbsPosition, fatherId);
                }

            }
        }

        primitiveToAdd->push_back((zPrimitive*)twinLineToAddToList);
    }
}

/**
    Saves a Node array specified in a JSON document into a zNode array. \n
    This funcion calls the primitives saving functions.
    @param nodeArray The array that contains the Nodes specified in the JSON document.
    @param fatherRelPosition The relative position of the container.
    @param fatherAbsPosition The absolute position of the container.
    @param fatherId The id of the container (zGNode).
*/

void JsonIO::readNode(QJsonArray nodeArray, QPoint fatherRelPosition, QPoint fatherAbsPosition, int fatherId, int wContainer, int hContainer)
{
    for (int nodeIndex = 0; nodeIndex < nodeArray.size(); ++nodeIndex) {

        QJsonObject nodeToCreate = nodeArray[nodeIndex].toObject();

        zNode* nodeToAdd = new zNode();

        nodeToAdd->setNId(nodeToCreate["id"].toInt());
        nodeToAdd->setFatherId(fatherId);

        //This coordinates are absolute position of the node in the canvas
        nodeToAdd->setXCord(nodeToCreate["x"].toDouble() + fatherAbsPosition.x() + fatherRelPosition.x());
        nodeToAdd->setYCord(nodeToCreate["y"].toDouble() + fatherAbsPosition.y() + fatherRelPosition.y());
        nodeToAdd->setFatherAbsXCord(fatherAbsPosition.x());
        nodeToAdd->setFatherAbsYCord(fatherAbsPosition.y());
        nodeToAdd->setFatherRelXCord(fatherRelPosition.x());
        nodeToAdd->setFatherRelYCord(fatherRelPosition.y());

        if( nodeToCreate["sameLineText"].toString() == "true")
            nodeToAdd->setTwoLinesText(true);

        nodeToAdd->setColor(nodeToCreate["color"].toString());
        nodeToAdd->setFillColor(nodeToCreate["fillcolor"].toString());

        //Information for layouts
        nodeToAdd->setMatrixRowsNumber(nodeToCreate["rows"].toInt());
        nodeToAdd->setMatrixColumnsNumber(nodeToCreate["columns"].toInt());
        nodeToAdd->setDefaultLayout(nodeToCreate["defaultLayout"].toString());
        nodeToAdd->setCustomLayout(nodeToCreate["customLayout"].toString());
        nodeToAdd->setCustomZoom(nodeToCreate["customZoom"].toDouble());

        //This variables give the node information in order to scale
        nodeToAdd->setWContainer(wContainer);
        nodeToAdd->setHContainer(hContainer);

        QPoint nFatherAbsPosition(nodeToAdd->getXCord(),nodeToAdd->getYCord());
        QPoint nFatherRelPosition(0,0);

        QList<zPrimitive*> *primitiveToAdd = new QList<zPrimitive*>();

        for(int keysIndex = 0; keysIndex < nodeToCreate.keys().size(); keysIndex++){

            if(nodeToCreate.keys()[keysIndex] == "rectangle"){
                QJsonArray rectangleArray = nodeToCreate[ nodeToCreate.keys()[keysIndex]].toArray();
                if(!rectangleArray.empty()){
                        readRectangle(rectangleArray, primitiveToAdd, nFatherRelPosition, nFatherAbsPosition, nodeToAdd->getNId());
                }
            }
            else if(nodeToCreate.keys()[keysIndex] == "ellipse"){
                QJsonArray ellipseArray = nodeToCreate[ nodeToCreate.keys()[keysIndex]].toArray();
                if(!ellipseArray.empty()){
                        readRectangle(ellipseArray, primitiveToAdd, nFatherRelPosition, nFatherAbsPosition, nodeToAdd->getNId());
                }
            }
            else if(nodeToCreate.keys()[keysIndex] == "text"){
                QJsonArray textArray = nodeToCreate[ nodeToCreate.keys()[keysIndex]].toArray();
                if(!textArray.empty()){
                        readText(textArray, primitiveToAdd,nFatherRelPosition);
                }
            }else if(nodeToCreate.keys()[keysIndex] == "twinLines"){
                QJsonArray twinLineArray = nodeToCreate[ nodeToCreate.keys()[keysIndex]].toArray();
                if(!twinLineArray.empty()){
                        readTwinLines(twinLineArray,primitiveToAdd, nFatherRelPosition, nFatherAbsPosition, nodeToAdd->getNId());
                }
            }else if(nodeToCreate.keys()[keysIndex] == "image"){
                QJsonArray imageArray = nodeToCreate[ nodeToCreate.keys()[keysIndex]].toArray();
                if(!imageArray.empty()){
                        readImage(imageArray, primitiveToAdd, nFatherRelPosition, nFatherAbsPosition, nodeToAdd->getNId());
                }
            }


       }

       nodeToAdd->setElementsList(primitiveToAdd);

       nodeList.push_back(nodeToAdd);
    }
}
/**
    Calls the readEdge() and readNode() functions. \n
*/
QList<zNode*> JsonIO::read()
{
    QJsonArray nodeArray = jsonObject["node"].toArray();
    QJsonArray edgeArray = jsonObject["edge"].toArray();
    if(!nodeArray.empty()){
        QPoint firstPosition(0,0);
        readNode(nodeArray, firstPosition,firstPosition, 0,-1,-1);
    }
    if(!edgeArray.empty()){
        readEdge(edgeArray);
    }

    return nodeList;

}
/**
    Saves a edge array specified in a JSON document into a zEdge array. \n
    @param edgeArray The array that contains the edges specified in the JSON document.
*/
void JsonIO::readEdge(QJsonArray edgeArray){
     for (int edgeIndex = 0; edgeIndex < edgeArray.size(); ++edgeIndex) {
        QJsonObject edgeToCreate = edgeArray[edgeIndex].toObject();
        int firstNodeId = edgeToCreate["nodeId1"].toInt();
        int secondNodeId = edgeToCreate["nodeId2"].toInt();
        int direction = edgeToCreate["direction"].toInt();
        qreal posmsg1 = edgeToCreate["posmsg1"].toDouble();
        qreal posmsg2 = edgeToCreate["posmsg2"].toDouble();
        QString message1 = edgeToCreate["message1"].toString();
        QString message2 = edgeToCreate["message2"].toString();
        QString arrowcolor = edgeToCreate["arrowcolor"].toString();
        QString msgcolor = edgeToCreate["msgcolor"].toString();
        zEdge * edgeToAdd = new zEdge(firstNodeId,secondNodeId,direction,message1,message2,
                                      posmsg1,posmsg2,arrowcolor,msgcolor);
        edgeList.push_back(edgeToAdd);
     }
}

JsonIO::~JsonIO()
{



}
