#ifndef JSONIO_H
#define JSONIO_H

#include "modelreader.h"
#include "QPoint"

class JsonIO : public ModelReader
{
public:
    JsonIO(QString filePath);
    ~JsonIO();

    QList<zNode*> read();
    void readRectangle(QJsonArray rectangleArray, QList<zPrimitive*> *primitiveToAdd,  QPoint fatherRelPosition, QPoint fatherAbsPosition, int fatherId);
    void readImage(QJsonArray imageArray, QList<zPrimitive*> *primitiveToAdd,  QPoint fatherRelPosition, QPoint fatherAbsPosition, int fatherId);
    void readEllipse(QJsonArray ellipseArray, QList<zPrimitive*> *primitiveToAdd, QPoint fatherRelPosition, QPoint fatherAbsPosition, int fatherId);
    void readNode(QJsonArray nodeArray, QPoint fatherRelPosition, QPoint fatherAbsPosition, int fatherId, int wContainer, int hContainer);
    void readEdge(QJsonArray edgeArray);
    void readText(QJsonArray textArray,QList<zPrimitive*> *primitiveToAdd,  QPoint fatherRelPosition);
    void readTwinLines(QJsonArray lineArray, QList<zPrimitive*> *primitiveToAdd,  QPoint fatherRelPosition, QPoint fatherAbsPosition, int fatherId);
private:
    QJsonObject jsonObject;
};

#endif // JSONIO_H
