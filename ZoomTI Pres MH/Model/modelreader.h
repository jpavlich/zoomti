#ifndef MODELREADER
#define MODELREADER

#include <QJsonObject>
#include <QList>
#include "../znode.h"
#include "../zedge.h"

class ModelReader
{
public:

    virtual QList<zNode*> read() = 0;

    virtual void setNodeList(QList<zNode*> nodeList){
        this->nodeList = nodeList;
    }
    virtual QList<zNode *> getNodeList(){
        return nodeList;
    }
    virtual void setEdgeList(QList<zEdge*> edgeList){
        this->edgeList = edgeList;
    }
    virtual QList<zEdge *> getEdgeList(){
        return edgeList;
    }

protected:
    QList<zNode*> nodeList;
    QList<zEdge*> edgeList;
};


#endif // MODELREADER

