#ifndef ZELLIPSE_H
#define ZELLIPSE_H
#include "zprimitive.h"

class zEllipse : public zPrimitive
{
public:
    zEllipse();
    ~zEllipse();
    void setWidth(double width);
    void setHeight(double height);

    double getWidth();
    double getHeight();

private:
    double width;
    double height;
};

#endif // ZELLIPSE_H
