#ifndef ZIMAGE_H
#define ZIMAGE_H
#include "zprimitive.h"

class zImage: public zPrimitive
{
public:
    zImage();
    ~zImage();
    void setWidth(double width);
    void setHeight(double height);
    void setImg(QString textImg);
    double getWidth();
    double getHeight();
    QPixmap getImg();

private:
    QPixmap *img;
    double width;
    double height;
};
#endif // ZIMAGE_H
