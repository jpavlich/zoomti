#ifndef ZTEXT_H
#define ZTEXT_H
#include "zprimitive.h"
#include "qstring.h"


class ztext: public zPrimitive
{
public:
    ztext();
    ~ztext();

    void setText(QString text);
    QString getText();
    void setAlign(QString align);
    QString getAlign();

private:
    QString text;
    QString align;

};

#endif // ZTEXT_H
