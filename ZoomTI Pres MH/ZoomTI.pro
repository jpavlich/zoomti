#-------------------------------------------------
#
# Project created by QtCreator 2015-02-19T14:37:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ZoomTI
TEMPLATE = app


SOURCES += main.cpp\
    graphicsview.cpp \
    Model/jsonio.cpp \
    Primitives/zrectangle.cpp \
    GraphicElements/zgnode.cpp \
    znode.cpp  \
    zedge.cpp \
    GraphicElements/zgedge.cpp \
    Primitives/ztext.cpp \
    Primitives/zellipse.cpp \
    Primitives/ztwinline.cpp \
    Layouts/zmatrix.cpp \
    Layouts/zcircle.cpp \
    Primitives/zimage.cpp
HEADERS  += \
    graphicsview.h \
    Model/jsonio.h \
    Primitives/zrectangle.h \
    Primitives/zprimitive.h \
    GraphicElements/zgnode.h \
    znode.h \
    Model/modelreader.h \
    zedge.h \
    GraphicElements/zgedge.h \
    Primitives/ztext.h \
    Primitives/zellipse.h \
    Primitives/ztwinline.h \
    Layouts/zmatrix.h \
    Layouts/zlayoutbase.h \
    Layouts/zcircle.h \
    Primitives/zimage.h

DISTFILES += \
    "Examples/exampleDataflowDiagram.json" \
    "Examples/exampleEcoreDiagram.json" \
    "Examples/exampleStateDiagram.json" \
    "Examples/exampleDataFlowImages.json" \
    "Examples/exampleUseCaseDiagramAndCommunication.json" \
    "Examples/exampleDeploymentDiagramComponentDiagramAndClassDiagram.json" \
    "Examples/templateTwinLines.json" \
    "Examples/templateEllipse.json" \
    "Examples/templateClass.json" \
    "Examples/templatePackage.json" \
    "Examples/templateState.json" \
    "Examples/templateCommunicationDiagram.json" \
    "Examples/templateComponent.json" \
    "Examples/templateNode.json" \
    "Examples/templateUseCase.json" \
    "Examples/templateUseCaseDiagram.json" \
    "Examples/templateUser.json" \
    "Images/1.png" \
    "Images/enterpriseData.png" \
    "Images/imageUser.gif"

CONFIG(release, debug|release) {
    DESTDIR = release
} else {
    DESTDIR = debug
}

win32 {
    copyfiles.commands += $(COPY_DIR) ..\\$${TARGET}\\Examples $${DESTDIR}\\Examples &
    copyfiles.commands += $(COPY_DIR) ..\\$${TARGET}\\Images $${DESTDIR}\\Images
}

QMAKE_EXTRA_TARGETS += copyfiles
POST_TARGETDEPS += copyfiles

RESOURCES += \
    images.qrc
