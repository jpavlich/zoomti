#include "graphicsview.h"

#include <QScrollBar>
#include <QTouchEvent>
#include <QDebug>
#include <QCursor>
#include <math.h>
#include <QDesktopWidget>
#include <QTimer>
/**
    Graphics view constructor. \n
    Private variables and flags of the GraphicsView Class are initialized here. \n
    @param scene The scene where every graphic class will be shown.

*/

GraphicsView::GraphicsView(QGraphicsScene *scene, QWidget *parent)
    : QGraphicsView(scene, parent), totalScaleFactor(/*0.*/1)
{

    conta = 0;
    pointId0 = -1;
    pointId1 = -1;
    point0Flag = false;
    point1Flag = false;
    backBut = true;
    //Zoom Animation

    animationTime = 1000;

    animationZoom = new QPropertyAnimation(this,"totalScaleFactor");
    animationZoom->setDuration(animationTime);
    animationZoom->setEasingCurve(QEasingCurve::Linear);

    animationCenterX = new QPropertyAnimation(this,"centerX");
    animationCenterX->setDuration(/*3**/animationTime/*/4*/);

    animationCenterY = new QPropertyAnimation(this,"centerY");
    animationCenterY->setDuration(/*3**/animationTime/*/4*/);


    //Paint all scene
    totalScaleFactor = 0.0001;
    setTransform(QTransform().fromScale(totalScaleFactor,totalScaleFactor));


    //Screen setup: full screen, drag, no scroll bars, multitouch support.
    viewport()->setAttribute(Qt::WA_AcceptTouchEvents);
    setDragMode(ScrollHandDrag);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
    setVerticalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
    setOptimizationFlag(QGraphicsView::DontAdjustForAntialiasing);
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);

    //Determine screen size
    QDesktopWidget widget;
    QRect mainScreenSize = widget.availableGeometry(widget.primaryScreen());
    this->screenHeight = mainScreenSize.height();
    this->screenWidth = mainScreenSize.width();

    //Slot for Load Another Model button
    loadAnotherModelButton = new QPushButton(">", this);
    loadAnotherModelButton->setGeometry(QRect(QPoint(screenWidth,screenHeight-40),QSize(35,35)));
    connect(loadAnotherModelButton, SIGNAL(released()),this,SLOT(handleLoadAnotherModelButton()));

    //Slot for Back to Main button
    backToMainButton = new QPushButton("VF", this);
    backToMainButton->setGeometry(QRect(QPoint((screenWidth/2)+45+17.5,screenHeight-40),QSize(35,35)));
    connect(backToMainButton, SIGNAL(released()),this, SLOT(handleBackToMainButton()));

    //Slot for Back One level button
    backOneLevelButton = new QPushButton("<", this);
    backOneLevelButton->setGeometry(QRect(QPoint(20,screenHeight-40),QSize(35,35)));
    connect(backOneLevelButton, SIGNAL(released()),this,SLOT(handleBackOneLevelButton()));

    //Slot for matrix Layout distribution
    matrixLayoutButton = new QPushButton("ML", this);
    matrixLayoutButton->setGeometry(QRect(QPoint((screenWidth/2)+17.5,screenHeight-40),QSize(35,35)));
    connect(matrixLayoutButton, SIGNAL (released()),this,SLOT(matrixLayout()));

    //Slot for circle Layout distribution
    circleLayoutButton = new QPushButton("CL", this);
    circleLayoutButton->setGeometry(QRect(QPoint((screenWidth/2)-45+17.5,screenHeight-40),QSize(35,35)));
    connect(circleLayoutButton, SIGNAL (released()),this,SLOT(circleLayout()));

    //Slot for layout timer
    layoutTimer = new QTimer(this);
    connect(layoutTimer, SIGNAL(timeout()), this, SLOT(layoutTimerU()));

    //Local variables to control behaviour
    centering = false;
    oldScaleFactor = 1;
    gNodeId = -1;

}

/**
   Handles the different events in the scene. \n
   @param event this function handles:\n
   1. Pressing the screen one time.\n
   2. Double pressing the screen.\n
   3. Touching sceen.\n
   4. End touching the screen.\n
   5. Keep touching the screen.\n


*/
bool GraphicsView::viewportEvent(QEvent *event)
{
    switch (event->type()) {
    //Disable selection at this point to allow drag while inside objects
    case QEvent::MouseButtonPress:
    {
        setInteractive(false);
        break;
    }
    //Enable selection at this point (in case of double click)
    case QEvent::MouseButtonRelease:
    {
        setInteractive(true);
        break;
    }
    //Simulates a Double Tap on the screen
    case QEvent::MouseButtonDblClick:
    {
        animationZoom->stop();
        animationCenterX->stop();
        animationCenterY->stop();

        QGraphicsItem* selectedIt =  (itemAt(QCursor::pos()));

        qreal actualScaleFactor = totalScaleFactor;

        QPointF centerAbs = mapToScene(this->width()/2, this->height()/2 );

        animationZoom->setStartValue(actualScaleFactor);
        animationCenterX->setStartValue(centerAbs.x());
        animationCenterY->setStartValue(centerAbs.y());


        if(selectedIt != NULL && selectedIt->data(3).toInt() == 1){
            if(totalScaleFactor - selectedIt->data(1).toReal() < -0.000001 || totalScaleFactor - selectedIt->data(1).toReal() > 0.000001){

                totalScaleFactor = selectedIt->data(1).toReal();
                animationZoom->setEndValue(totalScaleFactor);
                animationCenterX->setEndValue(selectedIt->data(4).toReal());
                animationCenterY->setEndValue(selectedIt->data(5).toReal());

                if(animationZoom->startValue() < animationZoom->endValue() ){
                     animationCenterX->setEasingCurve(QEasingCurve::OutExpo);
                     animationCenterY->setEasingCurve(QEasingCurve::OutExpo);
                }else{
                     animationCenterX->setEasingCurve(QEasingCurve::InExpo);
                     animationCenterY->setEasingCurve(QEasingCurve::InExpo);
                }

                animationCenterX->start();
                animationCenterY->start();
                animationZoom->start();

            }else if(totalScaleFactor - selectedIt->data(1).toReal() >= -0.000001 && totalScaleFactor - selectedIt->data(1).toReal() <= 0.000001 ){

                int nId = selectedIt->data(2).toInt();
                zGNode* zoomOut = graphicNodeMap[nId];

                    if(zoomOut->getFatherId() != 0){

                        zoomOut = graphicNodeMap[zoomOut->getFatherId()];
                        totalScaleFactor = zoomOut->getNodeScaleFactor();

                        animationZoom->setEndValue(totalScaleFactor);

                        animationCenterX->setEndValue(zoomOut->data(4).toReal());
                        animationCenterY->setEndValue(zoomOut->data(5).toReal());

                        if(animationZoom->startValue() < animationZoom->endValue() ){
                             animationCenterX->setEasingCurve(QEasingCurve::OutExpo);
                             animationCenterY->setEasingCurve(QEasingCurve::OutExpo);
                        }else{
                             animationCenterX->setEasingCurve(QEasingCurve::InExpo);
                             animationCenterY->setEasingCurve(QEasingCurve::InExpo);
                        }

                        animationCenterX->start();
                        animationCenterY->start();
                        animationZoom->start();

                    }else{

                        //Default Zoom but centered on the centered object
                        totalScaleFactor = 1;
                        animationZoom->setEndValue(totalScaleFactor);

                        animationCenterX->setEndValue(zoomOut->data(4).toReal());
                        animationCenterY->setEndValue(zoomOut->data(5).toReal());

                        if(animationZoom->startValue() < animationZoom->endValue() ){
                             animationCenterX->setEasingCurve(QEasingCurve::OutExpo);
                             animationCenterY->setEasingCurve(QEasingCurve::OutExpo);
                        }else{
                             animationCenterX->setEasingCurve(QEasingCurve::InExpo);
                             animationCenterY->setEasingCurve(QEasingCurve::InExpo);
                        }

                        animationCenterX->start();
                        animationCenterY->start();
                        animationZoom->start();

                    }
            }

        }

        break;
    }
        //Multitouch events handler
    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:
    {
        QTouchEvent *touchEvent = static_cast<QTouchEvent *>(event);
        QList<QTouchEvent::TouchPoint> touchPoints = touchEvent->touchPoints();

        //Restart Flags to see if one of the stored points not longer exists
        point0Flag = false;
        point1Flag = false;

        for(int touchListIndex=0; touchListIndex < touchPoints.size(); touchListIndex++){
            if(touchPoints[touchListIndex].id() == pointId0 ){
                point0Flag = true;
            }

            if(touchPoints[touchListIndex].id() == pointId1){
                point1Flag = true;
            }
        }

        if(!point0Flag){
            pointId0 = -1;
            centering = false;
        }

        if(!point1Flag){
            pointId1 = -1;
            centering = false;
        }

        //Restart end ------------------------------------------------------

        if(touchPoints.count() == 0 || touchPoints.count() == 1){
            centering = false;
            if(touchPoints[0].id() == pointId0){
                pointId1 = -1;
            }else{
                pointId0 = -1;
            }
        }
        if (touchPoints.count() == 2) {
            const QTouchEvent::TouchPoint &touchPoint0 = touchPoints.first();
            const QTouchEvent::TouchPoint &touchPoint1 = touchPoints.last();

            if(point0Flag == true && point1Flag == false){
                if(touchPoint0.id() == pointId0){
                     point0StartP = touchPoint0.pos();
                }else{
                     point0StartP = touchPoint1.pos();
                }
            }

            if(point1Flag == true && point0Flag == false){
                if(touchPoint0.id() == pointId1){
                     point1StartP = touchPoint0.pos();
                }else{
                     point1StartP = touchPoint1.pos();
                }
            }

            if(pointId0 == -1){
                if(pointId1 != touchPoint0.id()){
                    pointId0 = touchPoint0.id();
                    point0StartP = touchPoint0.pos();
                }else if(pointId1 != touchPoint1.id()){
                    pointId0 = touchPoint1.id();
                    point0StartP = touchPoint1.pos();
                }
            }

            if(pointId1 == -1){
                if(pointId0 != touchPoint0.id()){
                    pointId1 = touchPoint0.id();
                    point1StartP = touchPoint0.pos();
                }else if(pointId0 != touchPoint1.id()){
                    pointId1 = touchPoint1.id();
                    point1StartP = touchPoint1.pos();
                }
            }

            if(!centering){
                QCursor::setPos(point0StartP.x(),point0StartP.y());
                centering = true;
            }else{

                qreal currentScaleFactor =
                        QLineF(touchPoint0.pos(), touchPoint1.pos()).length()
                        / QLineF(point0StartP, point1StartP).length();

                bool haveToTransform = false;
                float d0 = QLineF(touchPoint0.pos(), touchPoint0.lastPos()).length();
                float d1 = QLineF(touchPoint1.pos(), touchPoint1.lastPos()).length();

                if( (qAbs(currentScaleFactor - oldScaleFactor) >= 0.005) && (d0 > 0.8f || d1 > 0.8f)) {
                    setOldScaleFactor(currentScaleFactor);
                    haveToTransform = true;
                }

                if (touchEvent->touchPointStates() & Qt::TouchPointReleased) {
                    // if one of the fingers is released, remember the current scale
                    // factor so that adding another finger later will continue zooming
                    // by adding new scale factor to the existing remembered value.
                    totalScaleFactor *= currentScaleFactor;
                    currentScaleFactor = 1;
                }

                if( haveToTransform ) {
                    setTransform(QTransform().fromScale(totalScaleFactor * currentScaleFactor, totalScaleFactor * currentScaleFactor));
                }
            }
            return true;
        }

        if(touchPoints.count() >= 3){
            if (touchEvent->touchPointStates() & Qt::TouchPointReleased) {

                bool shouldZoom0 = true;
                bool shouldZoom1 = true;

                for(int touchListIndex=0; touchListIndex < touchPoints.size(); touchListIndex++){
                    if(touchPoints[touchListIndex].id() == pointId0 && touchPoints[touchListIndex].state() != Qt::TouchPointReleased ){
                        shouldZoom0 = false;
                    }

                    if(touchPoints[touchListIndex].id() == pointId1 && touchPoints[touchListIndex].state() != Qt::TouchPointReleased ){
                        shouldZoom1 = false;
                    }
                }

                if(shouldZoom0 || shouldZoom1){
                    totalScaleFactor *= oldScaleFactor;
                    oldScaleFactor = 1;
                    centering =false;
                    setTransform(QTransform().fromScale(totalScaleFactor , totalScaleFactor ));
                }

            }
        }
    }
    default:
        break;
    }
    return QGraphicsView::viewportEvent(event);
}
/**
    Paints the entire model zGnode and zGEdge
*/
void GraphicsView::paintModel(){

    for(int nodeListIndex = (nodeList.size())-1; nodeListIndex >= 0; nodeListIndex--){

            qreal fatherScale = 1;
            qreal fatherXPos = 0;
            qreal fatherYPos = 0;
            if(graphicNodeMap.find( nodeList[nodeListIndex]->getFatherId()) != graphicNodeMap.end() ){
               fatherScale =  graphicNodeMap[nodeList[nodeListIndex]->getFatherId()]->scale();
               fatherXPos =   graphicNodeMap[nodeList[nodeListIndex]->getFatherId()]->getNodeXPos();
               fatherYPos =   graphicNodeMap[nodeList[nodeListIndex]->getFatherId()]->getNodeYPos();
            }

            zGNode * zgToAdd = new zGNode (nodeList[nodeListIndex],screenWidth, screenHeight, fatherScale, fatherXPos, fatherYPos);

            zgToAdd->setPos(zgToAdd->getNodeXPos(),zgToAdd->getNodeYPos());
            zgToAdd->setZValue(nodeList.size() - nodeListIndex);

            graphicNodeList.push_back(zgToAdd);
            graphicNodeMap[nodeList[nodeListIndex]->getNId()] = zgToAdd;

            this->scene()->addItem(graphicNodeList[graphicNodeList.size()-1]);
    }

    for(int edgeListIndex = 0; edgeListIndex < edgeList.size(); edgeListIndex++){
        zGEdge * zgToAdd = new zGEdge(edgeList[edgeListIndex],&graphicNodeMap);
        zgToAdd->setZValue(graphicNodeList.size()+1);
        zgToAdd->setActive(false);
        graphicEdgeList.push_back(zgToAdd);
        this->scene()->addItem(graphicEdgeList[edgeListIndex]);
    }

    for(int gNodeListIndex = (graphicNodeList.size())-1; gNodeListIndex >= 0; gNodeListIndex--){
        QString defLayout = graphicNodeList[gNodeListIndex]->getNode()->getDefaultLayout();

        if(defLayout != "null" ){
            if(defLayout == "matrix" ){
                this->gNodeId = graphicNodeList[gNodeListIndex]->getNode()->getNId();
                this->animate = false;
                matrixLayout();
            }
            else if(defLayout == "circle"){
                this->gNodeId = graphicNodeList[gNodeListIndex]->getNode()->getNId();
                this->animate = false;
                circleLayout();
            }
        }
    }

    //Timer to set the screen to the default position after being created
    QTimer::singleShot(250, this, SLOT(transformScreen()));

    layoutTimer->start(3000);

}
void GraphicsView :: setNodeList(QList<zNode*> nodeList){
    this->nodeList = nodeList;
}
void GraphicsView :: setEdgeList(QList<zEdge*> edgeList){
    this->edgeList = edgeList;
}

void GraphicsView :: setOldScaleFactor(qreal oldScaleFactor){
    this->oldScaleFactor = oldScaleFactor;
}

qreal GraphicsView :: getOldScaleFactor(){
    return this->oldScaleFactor;
}

void GraphicsView::setTotalScaleFactor(qreal totalScaleFactor){
    setTransform(QTransform().fromScale(totalScaleFactor,totalScaleFactor));
    centerOn(animationXCenter, animationYCenter);
    this->totalScaleFactor = totalScaleFactor;
}

qreal GraphicsView::getTotalScaleFactor(){
    return totalScaleFactor;
}

void GraphicsView::setCenterX(qreal centerX){
    animationXCenter = centerX;
    this->centerX = centerX;
}

qreal GraphicsView::getCenterX(){
        return this->centerX;
}

void GraphicsView::setCenterY(qreal centerY){
    animationYCenter = centerY;
    this->centerY = centerY;
}

qreal GraphicsView::getCenterY(){
        return this->centerY;
}

/**
   Handles the "Back to main" button.  \n
   The entire scene is showed after this function is called.

*/

void GraphicsView :: handleBackToMainButton(){

    animationZoom->stop();
    animationCenterX->stop();
    animationCenterY->stop();

    QPointF centerAbs = mapToScene(this->width()/2, this->height()/2 );
    qreal actualScaleFactor = totalScaleFactor;

    animationZoom->setStartValue(actualScaleFactor);
    animationCenterX->setStartValue(centerAbs.x());
    animationCenterY->setStartValue(centerAbs.y());

    animationZoom->setEndValue(1);
    animationCenterX->setEndValue(9100);
    animationCenterY->setEndValue(4900);

    if(animationZoom->startValue() < animationZoom->endValue() ){
         animationCenterX->setEasingCurve(QEasingCurve::OutExpo);
         animationCenterY->setEasingCurve(QEasingCurve::OutExpo);
    }else{
         animationCenterX->setEasingCurve(QEasingCurve::InExpo);
         animationCenterY->setEasingCurve(QEasingCurve::InExpo);
    }

    animationCenterX->start();
    animationCenterY->start();
    animationZoom->start();

}

/**
   Handles the "HBack one level" button.  \n
   Shows the container of a Node, if the canvas is the container, the entire scene will be shown.

*/
void GraphicsView :: handleBackOneLevelButton(){

    conta--;
    if( conta == 0 || conta == -1  )
        conta = 22;
    animationZoom->stop();
    animationCenterX->stop();
    animationCenterY->stop();



    qreal actualScaleFactor = totalScaleFactor;

    QPointF centerAbs = mapToScene(this->width()/2, this->height()/2 );

    animationZoom->setStartValue(actualScaleFactor);
    animationCenterX->setStartValue(centerAbs.x());
    animationCenterY->setStartValue(centerAbs.y());

    zGNode* zoomOut = graphicNodeMap[conta];




    //Default Zoom but centered on the centered object
    totalScaleFactor = zoomOut->getNodeScaleFactor();
    animationZoom->setEndValue(totalScaleFactor);

    animationCenterX->setEndValue(zoomOut->data(4).toReal());
    animationCenterY->setEndValue(zoomOut->data(5).toReal());

    if(animationZoom->startValue() < animationZoom->endValue() ){
        animationCenterX->setEasingCurve(QEasingCurve::OutExpo);
        animationCenterY->setEasingCurve(QEasingCurve::OutExpo);
    }else{
        animationCenterX->setEasingCurve(QEasingCurve::InExpo);
        animationCenterY->setEasingCurve(QEasingCurve::InExpo);
    }

    animationCenterX->start();
    animationCenterY->start();
    animationZoom->start();

    //conta--;



}
/**
   Handles the "Load another model" button.  \n
   Opens the file explorer so the user can open a JSON document with the diagram.
*/
/*
void GraphicsView :: handleLoadAnotherModelButton(){

    QString filePath = "";
    QStringList filesSelected = QFileDialog::getOpenFileNames(0,("Abrir modelo"),"/path/to/file/",("JSON Files (*.json)"));

    if(!filesSelected.isEmpty()){

        totalScaleFactor = 0.0001;
        setTransform(QTransform().fromScale(totalScaleFactor,totalScaleFactor));

        filePath = filesSelected[0];

        this->scene()->clear();

        graphicNodeList.clear();
        graphicEdgeList.clear();
        graphicNodeMap.clear();

        JsonIO model = JsonIO(filePath);
        nodeList = model.getNodeList();
        edgeList = model.getEdgeList();

        handleBackToMainButton();
        paintModel();
    }


}*/


void GraphicsView :: handleLoadAnotherModelButton(){
    conta++;
    if(conta == 23)
        conta = 1;
    animationZoom->stop();
    animationCenterX->stop();
    animationCenterY->stop();
    QGraphicsItem* selectedIt =  (itemAt(QCursor::pos()));

    qreal actualScaleFactor = totalScaleFactor;

    QPointF centerAbs = mapToScene(this->width()/2, this->height()/2 );

    animationZoom->setStartValue(actualScaleFactor);
    animationCenterX->setStartValue(centerAbs.x());
    animationCenterY->setStartValue(centerAbs.y());
    int nId = selectedIt->data(2).toInt();
    zGNode* zoomOut = graphicNodeMap[conta];




    //Default Zoom but centered on the centered object
    totalScaleFactor = zoomOut->getNodeScaleFactor();
    animationZoom->setEndValue(totalScaleFactor);

    animationCenterX->setEndValue(zoomOut->data(4).toReal());
    animationCenterY->setEndValue(zoomOut->data(5).toReal());

    if(animationZoom->startValue() < animationZoom->endValue() ){
        animationCenterX->setEasingCurve(QEasingCurve::OutExpo);
        animationCenterY->setEasingCurve(QEasingCurve::OutExpo);
    }else{
        animationCenterX->setEasingCurve(QEasingCurve::InExpo);
        animationCenterY->setEasingCurve(QEasingCurve::InExpo);
    }

    animationCenterX->start();
    animationCenterY->start();
    animationZoom->start();

}





/**
    Sets the view in the center of the scene.
 */
void GraphicsView::transformScreen(){
    totalScaleFactor = 1;
    setTransform(QTransform().fromScale(totalScaleFactor,totalScaleFactor));
    centerOn(9100, 4900);
}

/**
   Handles the "Matrix layout" button.  \n
   The node or scene is reorganized in a matrix layout.
*/
void GraphicsView::matrixLayout(){

        zmatrix matrix(2,3);

        matrix.setAutoResize(true);

        matrix.setGraphicNodeList(this->graphicNodeList);
        matrix.setGraphicNodeMap(this->graphicNodeMap);

        QPointF containerOriginPoint;
        bool shouldLayout = false;

        if(gNodeId != -1 ){

            matrix.setFatherId(gNodeId);

            zGNode* gNode = graphicNodeMap[gNodeId];

            int colNumber = gNode->getNode()->getMatrixColumnsNumber();
            int rowNumber = gNode->getNode()->getMatrixRowsNumber();
            if(colNumber != 0)
                matrix.setColumnNumber(colNumber);
            if(rowNumber != 0)
                matrix.setRowNumber(rowNumber);

            for( QMap<int, zGNode*>::iterator mapIt = graphicNodeMap.begin(); mapIt !=graphicNodeMap.end(); mapIt++){
                if( mapIt.value()->getFatherId() == gNodeId){

                    matrix.setContainerHeight(mapIt.value()->getNode()->getHContainer()*mapIt.value()->getFatherScaleVal() - 20*mapIt.value()->getFatherScaleVal());
                    matrix.setContainerWidth(mapIt.value()->getNode()->getWContainer()*mapIt.value()->getFatherScaleVal() - 10*mapIt.value()->getFatherScaleVal());

                    containerOriginPoint.setX(gNode->getNodeXPos() + 20*mapIt.value()->getFatherScaleVal() + mapIt.value()->getNode()->getFatherRelXCord()*mapIt.value()->getFatherScaleVal());
                    containerOriginPoint.setY(gNode->getNodeYPos() + 10*mapIt.value()->getFatherScaleVal() + mapIt.value()->getNode()->getFatherRelYCord()*mapIt.value()->getFatherScaleVal());

                    break;
                }
            }

            matrix.setContainerOriginPoint(containerOriginPoint);

            if(!animate){
                this->gNodeId = -1;
                this->animate = false;
                matrix.executeLayoutNoAnimation();
            }
        }
        else{
            int xCordCenter = this->width()/2;
            int yCordCenter = this->height()/2;

            QPointF centerAbs = mapToScene(xCordCenter, yCordCenter);
            qreal actualScaleFactor = totalScaleFactor;
            animationZoom->setStartValue(actualScaleFactor);
            animationCenterX->setStartValue(centerAbs.x());
            animationCenterY->setStartValue(centerAbs.y());

            QGraphicsItem* selectedIt =  (itemAt(xCordCenter,yCordCenter));
            bool foundZoomLevel = false;


            if(selectedIt != NULL){
                int nId = selectedIt->data(2).toInt();
                zGNode* zoomOut = graphicNodeMap[nId];

                while(!foundZoomLevel){
                    if(totalScaleFactor - selectedIt->data(1).toReal()  > -0.000001 ){

                        matrix.setFatherId(nId);
                        int colNumber = zoomOut->getNode()->getMatrixColumnsNumber();
                        int rowNumber = zoomOut->getNode()->getMatrixRowsNumber();
                        if(colNumber != 0)
                            matrix.setColumnNumber(colNumber);
                        if(rowNumber != 0)
                            matrix.setRowNumber(rowNumber);

                        for( QMap<int, zGNode*>::iterator mapIt = graphicNodeMap.begin(); mapIt !=graphicNodeMap.end(); mapIt++){
                            if( mapIt.value()->getFatherId() == nId){
                                matrix.setContainerHeight(mapIt.value()->getNode()->getHContainer()*mapIt.value()->getFatherScaleVal() - 20*mapIt.value()->getFatherScaleVal());
                                matrix.setContainerWidth(mapIt.value()->getNode()->getWContainer()*mapIt.value()->getFatherScaleVal() - 10*mapIt.value()->getFatherScaleVal());
                                shouldLayout = true;

                                containerOriginPoint.setX(zoomOut->getNodeXPos() + 20*mapIt.value()->getFatherScaleVal() + mapIt.value()->getNode()->getFatherRelXCord()*mapIt.value()->getFatherScaleVal());
                                containerOriginPoint.setY(zoomOut->getNodeYPos() + 10*mapIt.value()->getFatherScaleVal() + mapIt.value()->getNode()->getFatherRelYCord()*mapIt.value()->getFatherScaleVal());

                                break;
                            }
                        }
                        matrix.setContainerOriginPoint(containerOriginPoint);

                        foundZoomLevel = true;

                        totalScaleFactor = zoomOut->getNodeScaleFactor();

                        animationZoom->setEndValue(totalScaleFactor);

                        animationCenterX->setEndValue(zoomOut->data(4).toReal());
                        animationCenterY->setEndValue(zoomOut->data(5).toReal());
                        animationCenterX->setEasingCurve(QEasingCurve::InExpo);
                        animationCenterY->setEasingCurve(QEasingCurve::InExpo);

                    }else if(zoomOut->getFatherId() != 0){
                        zoomOut = graphicNodeMap[zoomOut->getFatherId()];
                    }else{
                        shouldLayout = true;
                        matrix.setFatherId(0);

                        matrix.setContainerHeight(this->screenHeight - 20);
                        matrix.setContainerWidth(this->screenWidth - 20);

                        containerOriginPoint.setX(9100 - this->screenWidth/2 + 120);
                        containerOriginPoint.setY(4900 - this->screenHeight/2 + 10);
                        matrix.setContainerOriginPoint(containerOriginPoint);

                        //Animation to watch more containers on screen

                        animationZoom->setEndValue(1);
                        animationCenterX->setEndValue(9100);
                        animationCenterY->setEndValue(4900);
                        animationCenterX->setEasingCurve(QEasingCurve::InExpo);
                        animationCenterY->setEasingCurve(QEasingCurve::InExpo);


                        foundZoomLevel = true;
                    }
                }


            }
            else
            {
                shouldLayout = true;
                matrix.setFatherId(0);

                matrix.setContainerHeight(this->screenHeight - 20);
                matrix.setContainerWidth(this->screenWidth - 20);

                containerOriginPoint.setX(9100 - this->screenWidth/2 + 120);
                containerOriginPoint.setY(4900 - this->screenHeight/2 + 10);
                matrix.setContainerOriginPoint(containerOriginPoint);

                //Animation to watch more containers on screen
                animationZoom->setEndValue(1);
                animationCenterX->setEndValue(9100);
                animationCenterY->setEndValue(4900);
                animationCenterX->setEasingCurve(QEasingCurve::InExpo);
                animationCenterY->setEasingCurve(QEasingCurve::InExpo);
            }
        }

        //container W, H y Punto

        if(shouldLayout || animate){

            if(shouldLayout){
                animationCenterX->start();
                animationCenterY->start();
                animationZoom->start();
            }

            this->gNodeId = -1;
            this->animate = false;
            matrix.executeLayout();
        }

}
/**
   Handles the "Circle layout" button.  \n
   The node or scene is reorganized in a circle layout.
*/
void GraphicsView::circleLayout(){

        zcircle circle;

        circle.setGraphicNodeList(this->graphicNodeList);
        circle.setGraphicNodeMap(this->graphicNodeMap);

        QPointF containerOriginPoint;
         bool shouldLayout = false;

        if(gNodeId != -1 ){

            circle.setFatherId(gNodeId);

            zGNode* gNode = graphicNodeMap[gNodeId];

            for( QMap<int, zGNode*>::iterator mapIt = graphicNodeMap.begin(); mapIt !=graphicNodeMap.end(); mapIt++){
                if( mapIt.value()->getFatherId() == gNodeId){

                    circle.setContainerHeight(mapIt.value()->getNode()->getHContainer()*mapIt.value()->getFatherScaleVal() - mapIt.value()->getFatherScaleVal());
                    circle.setContainerWidth(mapIt.value()->getNode()->getWContainer()*mapIt.value()->getFatherScaleVal() - mapIt.value()->getFatherScaleVal());

                    containerOriginPoint.setX(gNode->getNodeXPos() + mapIt.value()->getFatherScaleVal() + mapIt.value()->getNode()->getFatherRelXCord()*mapIt.value()->getFatherScaleVal());
                    containerOriginPoint.setY(gNode->getNodeYPos() + mapIt.value()->getFatherScaleVal() + mapIt.value()->getNode()->getFatherRelYCord()*mapIt.value()->getFatherScaleVal());

                    break;
                }
            }

            circle.setContainerOriginPoint(containerOriginPoint);

            if(!animate){
                this->gNodeId = -1;
                this->animate = false;
                circle.executeLayoutNoAnimation();
            }
        }
        else{
            int xCordCenter = this->width()/2;
            int yCordCenter = this->height()/2;

            QPointF centerAbs = mapToScene(xCordCenter, yCordCenter);
            qreal actualScaleFactor = totalScaleFactor;
            animationZoom->setStartValue(actualScaleFactor);
            animationCenterX->setStartValue(centerAbs.x());
            animationCenterY->setStartValue(centerAbs.y());

            QGraphicsItem* selectedIt =  (itemAt(xCordCenter,yCordCenter));
            bool foundZoomLevel = false;

            if(selectedIt != NULL){
                int nId = selectedIt->data(2).toInt();
                zGNode* zoomOut = graphicNodeMap[nId];

                while(!foundZoomLevel){
                    if(totalScaleFactor - selectedIt->data(1).toReal()  > -0.000001 ){

                        circle.setFatherId(nId);

                        for( QMap<int, zGNode*>::iterator mapIt = graphicNodeMap.begin(); mapIt !=graphicNodeMap.end(); mapIt++){
                            if( mapIt.value()->getFatherId() == nId){
                                circle.setContainerHeight(mapIt.value()->getNode()->getHContainer()*mapIt.value()->getFatherScaleVal() - mapIt.value()->getFatherScaleVal());
                                circle.setContainerWidth(mapIt.value()->getNode()->getWContainer()*mapIt.value()->getFatherScaleVal() - mapIt.value()->getFatherScaleVal());
                                shouldLayout = true;

                                containerOriginPoint.setX(zoomOut->getNodeXPos() + mapIt.value()->getFatherScaleVal() + mapIt.value()->getNode()->getFatherRelXCord()*mapIt.value()->getFatherScaleVal());
                                containerOriginPoint.setY(zoomOut->getNodeYPos() + mapIt.value()->getFatherScaleVal() + mapIt.value()->getNode()->getFatherRelYCord()*mapIt.value()->getFatherScaleVal());

                                break;
                            }
                        }
                        circle.setContainerOriginPoint(containerOriginPoint);

                        foundZoomLevel = true;

                        totalScaleFactor = zoomOut->getNodeScaleFactor();

                        animationZoom->setEndValue(totalScaleFactor);

                        animationCenterX->setEndValue(zoomOut->data(4).toReal());
                        animationCenterY->setEndValue(zoomOut->data(5).toReal());
                        animationCenterX->setEasingCurve(QEasingCurve::InExpo);
                        animationCenterY->setEasingCurve(QEasingCurve::InExpo);

                    }else if(zoomOut->getFatherId() != 0){
                        zoomOut = graphicNodeMap[zoomOut->getFatherId()];
                    }else{
                        shouldLayout = true;
                        circle.setFatherId(0);

                        circle.setContainerHeight(this->screenHeight );
                        circle.setContainerWidth(this->screenWidth );

                        containerOriginPoint.setX(9100 - this->screenWidth/2 );
                        containerOriginPoint.setY(4900 - this->screenHeight/2 );
                        circle.setContainerOriginPoint(containerOriginPoint);

                        //Animation to watch more containers on screen

                        animationZoom->setEndValue(1);
                        animationCenterX->setEndValue(9100);
                        animationCenterY->setEndValue(4900);
                        animationCenterX->setEasingCurve(QEasingCurve::InExpo);
                        animationCenterY->setEasingCurve(QEasingCurve::InExpo);


                        foundZoomLevel = true;
                    }
                }


            }
            else
            {
                shouldLayout = true;
                circle.setFatherId(0);

                circle.setContainerHeight(this->screenHeight);
                circle.setContainerWidth(this->screenWidth);

                containerOriginPoint.setX(9100 - this->screenWidth/2 );
                containerOriginPoint.setY(4900 - this->screenHeight/2 );
                circle.setContainerOriginPoint(containerOriginPoint);

                //Animation to watch more containers on screen
                animationZoom->setEndValue(1);
                animationCenterX->setEndValue(9100);
                animationCenterY->setEndValue(4900);
                animationCenterX->setEasingCurve(QEasingCurve::InExpo);
                animationCenterY->setEasingCurve(QEasingCurve::InExpo);
            }
            //container W, H y Punto
        }

        if(shouldLayout || animate){

            if(shouldLayout){
                animationCenterX->start();
                animationCenterY->start();
                animationZoom->start();
            }

            this->gNodeId = -1;
            this->animate = false;
            circle.executeLayout();
        }

}
/**
   This function is called a every certain amount of time. Checks all the nodes and ask if they need to be reorganiced. 
*/
void GraphicsView::layoutTimerU(){

    for(int gNodeListIndex = (graphicNodeList.size())-1; gNodeListIndex >= 0; gNodeListIndex--){
        QString custLayout = graphicNodeList[gNodeListIndex]->getNode()->getCustomLayout();

        if(custLayout != "null" ){
            if(custLayout == "matrix" ){
                if(graphicNodeList[gNodeListIndex]->getTrigger() == "matrix"){
                    graphicNodeList[gNodeListIndex]->setTrigger("null");
                    this->gNodeId = graphicNodeList[gNodeListIndex]->getNode()->getNId();
                    this->animate = true;
                    matrixLayout();
                }
            }
            else if(custLayout == "circle"){
                if(graphicNodeList[gNodeListIndex]->getTrigger() == "circle"){
                    graphicNodeList[gNodeListIndex]->setTrigger("null");
                    this->gNodeId = graphicNodeList[gNodeListIndex]->getNode()->getNId();
                    this->animate = true;
                    circleLayout();
                }
            }
        }
    }
}

