#ifndef ZCIRCLE_H
#define ZCIRCLE_H

#include "zlayoutbase.h"

class zcircle : public zLayoutBase
{

public:
    zcircle();
    ~zcircle();
    void executeLayout();
    void executeLayoutNoAnimation();
};

#endif // ZCIRCLE_H
