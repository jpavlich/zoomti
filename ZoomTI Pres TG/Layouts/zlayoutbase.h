#ifndef ZLAYOUTBASE
#define ZLAYOUTBASE

#include "../GraphicElements/zgnode.h"

class zLayoutBase
{

public:

    virtual QList<zGNode*>  getGraphicNodeList(){
        return graphicNodeList;
    }

    virtual void setGraphicNodeList( QList<zGNode*> graphicNodeList){
        this->graphicNodeList = graphicNodeList;
    }

    virtual QMap<int, zGNode* >  getGraphicNodeMap(){
        return graphicNodeMap;
    }

    virtual void setGraphicNodeMap( QMap<int, zGNode*>  graphicNodeMap){
        this->graphicNodeMap = graphicNodeMap;
    }

    virtual qreal getContainerWidth(){
        return containerWidth;
    }

    virtual void setContainerWidth( qreal containerWidth){
        this->containerWidth = containerWidth;
    }

    virtual qreal getContainerHeight(){
        return containerHeight;
    }

    virtual void setContainerHeight( qreal containerHeight){
        this->containerHeight = containerHeight;
    }

    virtual QPointF getContainerOriginPoint(){
        return this->containerOriginPoint;
    }

    virtual void setContainerOriginPoint( QPointF containerOriginPoint){
        this->containerOriginPoint = containerOriginPoint;
    }

    virtual int getFatherId(){
        return fatherId;
    }

    virtual void setFatherId(int fatherId){
        this->fatherId = fatherId;
    }

    virtual void executeLayout() = 0;
    virtual void executeLayoutNoAnimation() = 0;
    
    /**
       Translates the node to a new position with  its children.  \n
       This function is called by the layouts functions.
    */
    virtual void translateNode(int nodeId, qreal translateX, qreal translateY){

        //NodeXPos and NodeYPos are used by the edges
        graphicNodeMap[nodeId]->setNodeXPos(graphicNodeMap[nodeId]->pos().x() + translateX);
        graphicNodeMap[nodeId]->setNodeYPos(graphicNodeMap[nodeId]->pos().y() + translateY);

        //Data 4 5 6 7 are used on the double tap interaction
        graphicNodeMap[nodeId]->setData(4, graphicNodeMap[nodeId]->data(4).toFloat() + translateX);
        graphicNodeMap[nodeId]->setData(6, graphicNodeMap[nodeId]->data(6).toFloat() + translateX);
        graphicNodeMap[nodeId]->setData(5, graphicNodeMap[nodeId]->data(5).toFloat() + translateY);
        graphicNodeMap[nodeId]->setData(7, graphicNodeMap[nodeId]->data(7).toFloat() + translateY);

        graphicNodeMap[nodeId]->setAnimationFinalProperties(graphicNodeMap[nodeId]->pos().x(), graphicNodeMap[nodeId]->pos().x() + translateX,
                                                            graphicNodeMap[nodeId]->pos().y(), graphicNodeMap[nodeId]->pos().y() + translateY);
        graphicNodeMap[nodeId]->startAnimations();

        for(int graphicNodeListIndex=0; graphicNodeListIndex < graphicNodeList.size(); graphicNodeListIndex++){

            if( graphicNodeList[graphicNodeListIndex]->getFatherId() == nodeId){
                translateNode(graphicNodeList[graphicNodeListIndex]->getNode()->getNId(), translateX, translateY);
            }
        }
    }
    /**
       Handles the animation of the tanslateNode() function.  \n
    */
    virtual void translateNodeNoAnimation(int nodeId, qreal translateX, qreal translateY){

        //NodeXPos and NodeYPos are used by the edges
        graphicNodeMap[nodeId]->setNodeXPos(graphicNodeMap[nodeId]->pos().x() + translateX);
        graphicNodeMap[nodeId]->setNodeYPos(graphicNodeMap[nodeId]->pos().y() + translateY);

        //Data 4 5 6 7 are used on the double tap interaction
        graphicNodeMap[nodeId]->setData(4, graphicNodeMap[nodeId]->data(4).toFloat() + translateX);
        graphicNodeMap[nodeId]->setData(6, graphicNodeMap[nodeId]->data(6).toFloat() + translateX);
        graphicNodeMap[nodeId]->setData(5, graphicNodeMap[nodeId]->data(5).toFloat() + translateY);
        graphicNodeMap[nodeId]->setData(7, graphicNodeMap[nodeId]->data(7).toFloat() + translateY);

        graphicNodeMap[nodeId]->setPos(graphicNodeMap[nodeId]->pos().x() + translateX,
                                                            graphicNodeMap[nodeId]->pos().y() + translateY);

        for(int graphicNodeListIndex=0; graphicNodeListIndex < graphicNodeList.size(); graphicNodeListIndex++){

            if( graphicNodeList[graphicNodeListIndex]->getFatherId() == nodeId){
                translateNodeNoAnimation(graphicNodeList[graphicNodeListIndex]->getNode()->getNId(), translateX, translateY);
            }
        }
    }



protected:

    QList<zGNode*>  graphicNodeList;
    QMap<int, zGNode* >  graphicNodeMap;

    qreal containerWidth;
    qreal containerHeight;
    QPointF containerOriginPoint;
    int fatherId;

};

#endif // ZLAYOUTBASE

