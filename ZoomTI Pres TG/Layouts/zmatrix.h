#ifndef ZMATRIX_H
#define ZMATRIX_H

#include "zlayoutbase.h"


class zmatrix : public zLayoutBase
{

public:
    zmatrix( int rowNumber, int columnNumber);
    ~zmatrix();

    void executeLayout();
    void executeLayoutNoAnimation();
    void setAutoResize(bool autoResize);
    bool getAutoResize();
    void setColumnNumber(int columnNumber);
    void setRowNumber(int rowNumber);

private:
    int rowNumber;
    int columnNumber;
    bool autoResize;


};

#endif // ZMATRIX_H
