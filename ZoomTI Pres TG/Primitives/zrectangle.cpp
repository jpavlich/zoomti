#include "zrectangle.h"

zRectangle::zRectangle()
{
    this->radius = 0;
}

zRectangle::~zRectangle()
{

}
void zRectangle :: setHeight(double height){
    this->height = height;
}
void zRectangle :: setWidth(double width){
    this->width = width;
}
void zRectangle :: setRadius(double radius){
    this->radius = radius;
}

void zRectangle :: setCurveTopLeft(bool curveTopLeft){
    this->curveTopLeft = curveTopLeft;
}

void zRectangle :: setCurveTopRight(bool curveTopRight){
    this->curveTopRight = curveTopRight;
}

void zRectangle :: setCurveBottomLeft(bool curveBottomLeft){
    this->curveBottomLeft = curveBottomLeft;
}

void zRectangle :: setCurveBottomRight(bool curveBottomRight){
    this->curveBottomRight = curveBottomRight;
}


double zRectangle :: getHeight(){
    return height;
}
double zRectangle :: getWidth(){
    return width;
}
qreal zRectangle :: getRadius(){
    return radius;
}

bool zRectangle :: getCurveTopLeft(){
    return curveTopLeft;
}

bool zRectangle :: getCurveTopRight(){
    return curveTopRight;
}

bool zRectangle :: getCurveBottomLeft(){
    return curveBottomLeft;
}

bool zRectangle :: getCurveBottomRight(){
    return curveBottomRight;
}

