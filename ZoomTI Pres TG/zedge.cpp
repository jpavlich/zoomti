#include "zedge.h"

zEdge::zEdge(int idFirstNode, int idSecondNode, int direction,
             QString message1, QString message2,
             qreal posmsg1, qreal posmsg2, QString arrowcolor, QString msgcolor ){

    this->idFirstNode = idFirstNode;
    this->idSecondNode = idSecondNode;
    this->direction = direction;
    this->message1 = message1;
    this->message2 = message2;
    this->posmsg1 = posmsg1;
    this->posmsg2 = posmsg2;
    this->arrowcolor = arrowcolor;
    this->msgcolor = msgcolor;
}

zEdge::~zEdge()
{

}
int zEdge ::getIdFirstNode(){
    return this->idFirstNode;
}
int zEdge ::getIdSecondNode(){
    return this->idSecondNode;
}
int zEdge ::getDirection(){
    return this->direction;
}
QString zEdge ::getMessage1(){
    return this->message1;
}
QString zEdge ::getMessage2(){
    return this->message2;
}
QString zEdge ::getArrowColor(){
    return this->arrowcolor;
}
QString zEdge ::getMsgColor(){
    return this->msgcolor;
}
qreal zEdge ::getPosmsg1(){
    return this->posmsg1;
}
qreal zEdge ::getPosmsg2(){
    return this->posmsg2;
}
