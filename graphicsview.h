
#pragma once
#include <QGraphicsView>
#include "GraphicElements/zgnode.h"
#include "GraphicElements/zgedge.h"
#include "Model/jsonio.h"
#include "Layouts/zmatrix.h"
#include "Layouts/zcircle.h"

#include <QVector>
#include <QPushButton>
#include <QStack>
#include <QPair>
#include <qwidget.h>
#include <qfiledialog.h>


class GraphicsView : public QGraphicsView
{
    Q_OBJECT
    Q_PROPERTY(qreal totalScaleFactor READ getTotalScaleFactor WRITE setTotalScaleFactor )
    Q_PROPERTY(qreal centerX READ getCenterX WRITE setCenterX )
    Q_PROPERTY(qreal centerY READ getCenterY WRITE setCenterY )


public:
    GraphicsView(QGraphicsScene *scene = 0, QWidget *parent = 0);

    bool viewportEvent(QEvent *event) Q_DECL_OVERRIDE;
    void paintModel();

    //Getters and Setters
    void setNodeList(QList<zNode*> nodeList);
    void setEdgeList(QList<zEdge*> edgeList);
    void setOldScaleFactor(qreal oldScaleFactor);
    qreal getOldScaleFactor();
    void setTotalScaleFactor(qreal totalScaleFactor);
    qreal getTotalScaleFactor();
    void setCenterX(qreal centerX);
    qreal getCenterX();
    void setCenterY(qreal centerY);
    qreal getCenterY();

    void animateOneLevel();
    void animationOneLevel(qreal newFactor);

private slots:

    void handleLoadAnotherModelButton();
    void handleBackToMainButton();
    void handleBackOneLevelButton();
    void transformScreen();
    void matrixLayout();
    void circleLayout();
    void layoutTimerU();

private:

    qreal totalScaleFactor;
    qreal centerX;
    qreal centerY;

    QList<zNode*> nodeList;
    QList<zEdge*> edgeList;
    QList<zGNode*> graphicNodeList;
    QList<zGEdge*> graphicEdgeList;
    QMap<int, zGNode* > graphicNodeMap;

    QVector<qreal> zoomAvg;
    bool centering;
    qreal oldScaleFactor;

    //Screen Buttons
    QPushButton *loadAnotherModelButton;
    QPushButton *backToMainButton;
    QPushButton *backOneLevelButton;
    QPushButton *matrixLayoutButton;
    QPushButton *circleLayoutButton;

    int screenHeight;
    int screenWidth;

    qreal animationXCenter;
    qreal animationYCenter;
    int animationTime;
    QPropertyAnimation *animationZoom;
    QPropertyAnimation *animationCenterX;
    QPropertyAnimation *animationCenterY;

    int pointId0;
    int pointId1;
    QPointF point0StartP;
    QPointF point1StartP;
    bool point0Flag;
    bool point1Flag;

    qreal savedF;

    int gNodeId;
    bool animate;

    QTimer *layoutTimer;

};
