#include "graphicsview.h"
#include <QApplication>
#include "Model/jsonio.h"
#include <qdebug.h>
#include <qfiledialog.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QGraphicsScene scene;
    scene.setSceneRect(0, 0, 18200, 9800);
    scene.setItemIndexMethod(QGraphicsScene::NoIndex);

    //FileChooser
    QString filePath = "test.json";
    QStringList filesSelected = QFileDialog::getOpenFileNames(0,("Abrir modelo"),"/path/to/file/",("JSON Files (*.json)"));

    if(!filesSelected.isEmpty())
        filePath = filesSelected[0];

    //Model Loader
    JsonIO model = JsonIO(filePath);

    //Scene Bulder
    GraphicsView view(&scene);
    view.setNodeList(model.getNodeList());
    view.setEdgeList(model.getEdgeList());
    view.paintModel();
    view.setRenderHint(QPainter::Antialiasing);
    view.setCacheMode(QGraphicsView::CacheBackground);
    view.setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    view.showFullScreen();

    return a.exec();
}
