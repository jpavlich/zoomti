#include "znode.h"
#include <QDebug>

zNode::zNode()
{
    this->twoLinesText = false;

    this->defaultLayout = "null";
    this->customLayout = "null";
    this->customZoom = -1;
}

zNode::~zNode()
{

}

void zNode::setElementsList(QList<zPrimitive*> *elementsList){
    this->elementsList = elementsList;
}

QList<zPrimitive*>* zNode::getElementsList(){
    return elementsList;
}

void zNode :: printNode(){

    if(!this->getElementsList()->empty())
    {

    }

}

qreal zNode::getXCord(){
    return this->xCord;
}

qreal zNode::getYCord(){
    return this->yCord;
}

int zNode::getNId(){
    return this->nId;
}

int zNode::getFatherId(){
    return this->fatherId;
}

int zNode::getHContainer(){
    return this->hContainer;
}

int zNode::getWContainer(){
    return this->wContainer;
}
QString zNode::getFillColor(){
    return this->fillColor;
}
QString zNode::getColor(){
    return this->color;
}

qreal zNode::getFatherAbsXCord(){
    return this->fatherAbsXCord;
}

qreal zNode::getFatherAbsYCord(){
    return this->fatherAbsYCord;
}

qreal zNode::getFatherRelXCord(){
    return this->fatherRelXCord;
}

qreal zNode::getFatherRelYCord(){
    return this->fatherRelYCord;
}

int zNode::getMatrixColumnsNumber(){
    return this->matrixColumnsNumber;
}

int zNode::getMatrixRowsNumber(){
    return this->matrixRowsNumber;
}

bool zNode::getTwoLinesText(){
    return this->twoLinesText;
}

QString zNode::getDefaultLayout(){
    return this->defaultLayout;
}

QString zNode::getCustomLayout(){
    return this->customLayout;
}

qreal zNode::getCustomZoom(){
    return this->customZoom;
}

void zNode::setXCord(qreal xCord){
    this->xCord = xCord;
}

void zNode::setYCord(qreal yCord){
    this->yCord = yCord;
}

void zNode::setNId(int nId){
    this->nId = nId;
}

void zNode::setFatherId(int fatherId){
    this->fatherId = fatherId;
}

void zNode::setWContainer(int wContainer){
    this->wContainer = wContainer;
}

void zNode::setHContainer(int hContainer){
    this->hContainer = hContainer;
}
void zNode::setFillColor(QString fillColor){
    this->fillColor = fillColor;
}
void zNode::setColor(QString color){
    this->color = color;
}

void zNode::setFatherRelXCord(qreal fatherRelXCord){
    this->fatherRelXCord = fatherRelXCord;
}

void zNode::setFatherRelYCord(qreal fatherRelYCord){
    this->fatherRelYCord = fatherRelYCord;
}

void zNode::setFatherAbsXCord(qreal fatherAbsXCord){
    this->fatherAbsXCord = fatherAbsXCord;
}

void zNode::setFatherAbsYCord(qreal fatherAbsYCord){
    this->fatherAbsYCord = fatherAbsYCord;
}

void zNode::setMatrixColumnsNumber(int matrixColumnsNumber){
    this->matrixColumnsNumber = matrixColumnsNumber;
}

void zNode::setMatrixRowsNumber(int matrixRowsNumberx){
    this->matrixRowsNumber = matrixRowsNumberx;
}

void zNode::setTwoLinesText(bool twoLinesText){
    this->twoLinesText = twoLinesText;
}

void zNode::setDefaultLayout(QString defaultLayout){
    this->defaultLayout = defaultLayout;
}

void zNode::setCustomLayout(QString customLayout){
    this->customLayout = customLayout;
}

void zNode::setCustomZoom(qreal customZoom){
    this->customZoom = customZoom;
}


