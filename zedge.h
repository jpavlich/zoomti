    #ifndef ZEDGE_H
#define ZEDGE_H
#include "GraphicElements/zgnode.h"

class zEdge {
public:
    zEdge(int idFirstNode, int idSecondNode, int direction, QString message1, QString message2, qreal posmsg1,
          qreal posmsg2, QString arrowcolor, QString msgcolor);
    ~zEdge();
    int getIdFirstNode();
    int getIdSecondNode();
    int getDirection();
    qreal getPosmsg1();
    qreal getPosmsg2();
    QString getMessage1();
    QString getMessage2();
    QString getMsgColor();
    QString getArrowColor();
private:
    int idFirstNode;
    int idSecondNode;
    int direction;
    qreal posmsg1;
    qreal posmsg2;
    QString message1;
    QString message2;
    QString msgcolor;
    QString arrowcolor;


};

#endif // ZEDGE_H
