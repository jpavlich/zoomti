#ifndef ZNODE_H
#define ZNODE_H

#include "Primitives/zprimitive.h"
#include "Primitives/zrectangle.h"
#include "Primitives/ztext.h"
#include "Primitives/zellipse.h"
#include "Primitives/ztwinline.h"
#include "Primitives/zimage.h"
#include <QList>
#include <QString>


class zNode
{
public:
    zNode();
    ~zNode();
    void setElementsList(QList<zPrimitive*> *elementsList);
    QList<zPrimitive*> *getElementsList();
    void printNode();

    qreal getXCord();
    qreal getYCord();
    int getNId();
    int getFatherId();
    int getWContainer();
    int getHContainer();

    qreal getFatherRelXCord();
    qreal getFatherRelYCord();
    qreal getFatherAbsXCord();
    qreal getFatherAbsYCord();
    bool getTwoLinesText();

    QString getColor();
    QString getFillColor();

    int getMatrixRowsNumber();
    int getMatrixColumnsNumber();
    QString getDefaultLayout();
    QString getCustomLayout();
    qreal getCustomZoom();

    void setXCord(qreal xCord);
    void setYCord(qreal yCord);
    void setNId(int nId);
    void setFatherId(int fatherId);
    void setWContainer(int wContainer);
    void setHContainer(int hContainer);
    void setFillColor(QString alpha);
    void setColor(QString color);
    void setFatherRelXCord(qreal fatherRelXCord);
    void setFatherRelYCord(qreal fatherRelYCord);
    void setFatherAbsXCord(qreal fatherAbsXCord);
    void setFatherAbsYCord(qreal fatherAbsYCord);
    void setMatrixRowsNumber(int matrixRowsNumberx);
    void setMatrixColumnsNumber(int matrixColumnsNumber);
    void setTwoLinesText(bool twoLinesText);
    void setDefaultLayout(QString defaultLayout);
    void setCustomLayout(QString customLayout);
    void setCustomZoom(qreal customZoom);

private:

    QList<zPrimitive*> *elementsList;
    QList<zNode*> *insideNodeList;

    //Absolute coords, Relative Father Coords and Absolute Father Coords
    qreal xCord;
    qreal yCord;
    qreal fatherRelXCord;
    qreal fatherRelYCord;
    qreal fatherAbsXCord;
    qreal fatherAbsYCord;

    int nId;
    int fatherId;
    int wContainer;
    int hContainer;
    QString fillColor;
    QString color;

    int matrixRowsNumber;
    int matrixColumnsNumber;

    bool twoLinesText;
    QString defaultLayout;
    QString customLayout;
    qreal customZoom;

};

#endif // ZNODE_H
